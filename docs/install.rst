Installation
============

Clone or download `modulecmd`_ and source the appropriate setup script in your shell's initialization script.

--------------------
Shell Initialization
--------------------

`modulecmd`_ initialization scripts define the ``module`` command that executes `modulecmd`_ modules.  Because of differences between shells, users must ``source`` the appropriate initialization script in their shell's startup script.  The `modulecmd`_ initialization scripts are located in ``${MODULECMD_DIR}/scripts``, where ``${MODULECMD_DIR}`` is the path to the directory where `modulecmd`_ was cloned or downloaded.

----
Bash
----

In your ``.bashrc``, add the following:

.. code-block:: console

  $ source ${MODULECMD_DIR}/scripts/moduleinit.bash

---------------------
Test the installation
---------------------

Test the installation with `modulecmd`_'s subcommand ``test``:

.. code-block:: console

  $ module test

which will report passed and failed tests.  Failed tests are considered a bug, please report them to the `modulecmd`_ developers.

---------------------
Module initialization
---------------------

While not necessary, adding a call to the ``init`` subcommand, just after ``source``\ ing the `modulecmd`_ initialization script in your shell's startup script, will perform some useful initializations:


.. code-block:: console

  $ module init [-p=<MODULEPATH>]

The optional ``-p`` flag is used to set an initial ``MODULEPATH``.  See basic-usage_ for a description of the ``MODULEPATH``.


.. _modulecmd: https://www.gitlab.com/tjfulle/modulecmd
