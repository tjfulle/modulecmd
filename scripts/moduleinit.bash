#!/bin/bash
_pathadd() {
  # If no variable name is supplied, just append to PATH
  # otherwise append to that variable.
  _pa_varname=PATH
  _pa_new_path="$1"
  if [ -n "$2" ]; then
    _pa_varname="$1"
    _pa_new_path="$2"
  fi
  # Do the actual prepending here.
  eval "_pa_oldvalue=\${${_pa_varname}:-}"
  _pa_canonical=":$_pa_oldvalue:"
  if [ -d "$_pa_new_path" ] && \
   [ "${_pa_canonical#*:${_pa_new_path}:}" = "${_pa_canonical}" ];
  then
    if [ -n "$_pa_oldvalue" ]; then
      eval "export $_pa_varname=\"$_pa_new_path:$_pa_oldvalue\""
    else
      export $_pa_varname="$_pa_new_path"
    fi
  fi
}

# Identify and lock the python interpreter
PREFERRED_PYTHONS="python3 python /usr/libexec/platform-python"
for cmd in "${MODULECMD_PYTHON:-}" ${PREFERRED_PYTHONS}; do
  if command -v > /dev/null "$cmd"; then
    export MODULECMD_PYTHON="$(command -v "$cmd")"
    _pybin="$(dirname $MODULECMD_PYTHON)"
    _pathadd PATH $_pybin
    break
  fi
done

_pyver=$($MODULECMD_PYTHON -V 2>&1 | sed 's/.* \([0-9]\).\([0-9]*\).*/\1\2/')
if [ "${_pyver}" -lt "36" ]; then
  full=$($MODULECMD_PYTHON -V 2>&1)
  echo "$0: Python 3.6 or greater required, found ${full}" 1>&2
  return 1
fi

_source_file="${BASH_SOURCE[0]:-}"
_dir="$(cd "$(dirname $_source_file)" > /dev/null && pwd)"
_prefix="$(cd "$(dirname $_dir)" > /dev/null && pwd)"
_pathadd PATH "${_dir}"
export MODULECMD_PREFIX=${_prefix}

module()
{
  eval $(PYTHONPATH="${_prefix}:${PYTHONPATH}" ${MODULECMD_PYTHON} -m modulecmd "$@")
}
