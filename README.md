# modulecmd

`modulecmd` is an environment module system inspired by TCL [Environment Modules](http://modules.sourceforge.net) and Lua [lmod](https://lmod.readthedocs.io/en/latest/) and implemented in Python.  `modulecmd` requires Python 3.6+.

`modulecmd` provides a framework for processing module files written in Python.  Additionally, `modulecmd` processes TCL modules by wrapping the TCL module command.  TCL compatibility requires that `tclsh` be found on the user's `PATH` (the TCL module command is provided by `modulecmd`).

## Why another module system?

TCL modules and the associated TCL module command are ubiquitous on most HPC
systems I work on, but I don't work in/write TCL; `lmod` adds unique
capabilities to the TCL module command and is actively developed, but is not
available on most of the machines I work on and requires an extra build step;
but mostly, I prefer to write python and every system on which I work has a
Python installation.

## What are environment modules?

Environment modules, or just modules, are files containing commands that, when
processed by the module framework, modify the current shell's environment.
Modules allow users to dynamically modify their environment.  For example, a
module file (call it `foo.py`) containing the following command, sets the
environment variable `FOO` to the value `BAR`

```python
setenv('FOO', 'BAR')
```

The module is loaded in to the environment with the following command

```sh
module load foo
```

(note the absence of the extension `.py`).  With modules, environment variables,
path-like variables, aliases, and shell functions can be set/unset/modified.

### Why environment modules?

Consider the workflow of a developer working on two projects requiring compilers
`xcc-1.0` and `xcc-2.0`, respectively.  Binaries created by each compiler are
incompatible with the other.  The developer creates two `modulecmd` modules
containing the following instructions

`xcc-1.0.py`:

```python
append_path('PATH', '/path/to/xcc-1.0')
```

and

`xcc-2.0.py`:

```python
append_path('PATH', '/path/to/xcc-2.0')
```

When the developer works on the project requiring `xcc-1.0`, the appropriate
module is be loaded, putting the the compiler in the environment's `PATH`.
`xcc-2.0` can later be accessed by unloading module `xcc-1.0` and loading
`xcc-2.0`.

This example is meant to merely demonstrate a simplistic usage of `modulecmd`
modules.  This document describes the `modulecmd` module framework in more
detail.

## A note on terminology

In the context of module files, the term module is different from the standard
Python definition.  In Python, a module is a Python file containing python
definitions and statements.  Python modules can be imported into other modules.
In contrast, a `modulecmd` environment module, while a python file containing
definitions and statements, is not intended to be imported by other Python
modules.  Rather, the `modulecmd` module is executed by `modulecmd` using the
the `modulecmd` framework.  Commands in a `modulecmd` module are translated and
injected in to the user's environment.

## Installation

Clone or download `modulecmd`.  In your `.bashrc`, add the following:

```
${MODULECMD_DIR}/scripts/module init bash
```

where `${MODULECMD_DIR}` is the path to the directory where `modulecmd` is
cloned.  Start a new shell and the ``module`` command will be available.

## The module command

The `modulecmd` initialization defines the `module` shell function that executes
`modulecmd` modules.  Valid subcommands of `module` are:

```sh
    reset               Reset environment to initial environment
    setenv              Set environment variables
    avail               Display available modules
    list                Display loaded modules
    edit                Edit module files
    show                Show module[s]
    cat                 cat module[s] to screen
    load                Load module[s]
    unload              Unload module[s]
    reload              Reload module[s]
    use                 Add directory[s] to MODULEPATH
    unuse               Remove directory[s] from MODULEPATH
    purge               Unload all modules
    swap                Swap modules
    whatis              Display module whatis string
    help                Display module help string
```

## modulecmd module files

`modulecmd` module files are executed by the `modulecmd` framework.  `modulecmd`
executes module files in an environment providing the following commands:

- `getenv(name)`: Get the value of environment variable given by `name`.
   Returns `None` if `name` is not defined.
- `gethostname()`: Get the value of the host name of the sytem.
- `mode()`: Return the active mode.  One of `"load"` or `"unload"`

### Message logging

The `modulecmd.xio` logging library is used for logging messages to the console.

- `xio.debug(message)`: Log a debug message to the console.
- `xio.info(message)`: Log an informational message to the console.
- `xio.warn(message)`: Log a warning message to the console.
- `xio.error(message)`: Log an error message to the console.
- `xio.die(message)`: Log an error message to the console and quit.

## Environment Modification

- `setenv(variable, value)`: Set the environment variable `variable` to `value`.
- `unsetenv(variable)`: Unset the environment variable `variable`.

- `set_alias(name, value)`: Set the alias `name` to `value`.
- `unset_alias(name)`: Unset the alias given by `name`.

- `set_shell_function(name, value)`: Set the shell function `name` to `value`.
- `unset_shell_function(name, value)`: Unset the shell function `name`

- `prepend_path(pathname, value)`: Prepend `value` to path-like variable `pathname`.
- `append_path(pathname, value)`: Append `value` to path-like variable `pathname`.
- `remove_path(pathname, value)`: Remove `value` from path-like variable `pathname`.

## Interaction with Other Modules

- `prereq(name)`: Module `name` is a prerequisite of this module.  If `name` is
   not loaded, `modulecmd` will quit.
- `prereq_any(*names)`: Any one of `names` is a prerequisite of this module.  If
   none of `names` is not loaded, `modulecmd` will quit.
- `load(name)`: Load the module `name`.
- `load_first(*names)`: Load the first module in `names`.
- `unload(name)`: Unload the module `name`.

## Other Commands

- `family(name)`: Set the name of the module's family.
- `execute(command)`: Execute `command` in the current shell.
- `whatis(string)`: Store `string` as an informational message describing this module.

# Other Objects/Constants

- `self`: Reference to this modules object.
- `is_darwin`: Boolean.  `True` if the system is Darwin.  `False` otherwise.
