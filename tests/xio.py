import pytest
import modulecmd.xio as xio


def test_xio_colify():
    elements = list("abcdefghijklmnopqrstuvwxyz")
    s = xio.colify(elements, width=10)
    assert len(s.split("\n")) == 9
    xio.colify([])


def test_xio_terminal_size():
    xio.terminal_size()


def test_xio_debug():
    orig = xio.set_debug(True)
    xio.debug("here is a debug message")
    xio.set_debug(orig)


def test_xio_trace():
    orig = xio.set_log_level(xio.TRACE)
    xio.trace("here is a trace message")
    xio.set_log_level(orig)


def test_xio_die():
    with pytest.raises(SystemExit):
        xio.die("here is a die message")


def test_xio_get_log_level_by_name():
    assert xio.get_log_level_by_name("trace") == xio.TRACE
    assert xio.get_log_level_by_name("debug") == xio.DEBUG
    assert xio.get_log_level_by_name("info") == xio.INFO
    assert xio.get_log_level_by_name("warn") == xio.WARN
    assert xio.get_log_level_by_name("error") == xio.ERROR


def test_xio_set_output_stream():
    xio.set_output_stream(xio.output_stream)


def test_xio_set_log_level_by_name():
    orig = xio.LOG_LEVEL
    xio.set_log_level_by_name("error")
    xio.set_log_level(orig)


def test_xio_print():
    xio.print("")
