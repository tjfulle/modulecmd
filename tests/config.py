import os
from io import StringIO
import modulecmd.config as config


def test_config_get_config_dir():
    var1 = "MODULECMD_CONFIG_DIR"
    var2 = "XDG_CONFIG_HOME"

    d1 = os.environ.pop(var1, None)
    d2 = os.environ.pop(var2, None)

    assert config.get_config_dir() == os.path.expanduser("~/.config/modulecmd")

    os.environ[var1] = "~/a/path"
    assert config.get_config_dir() == os.path.expanduser("~/a/path")
    os.environ.pop(var1)

    os.environ[var2] = "~/b/path"
    assert config.get_config_dir() == os.path.expanduser("~/b/path")
    os.environ.pop(var2)

    if d1 is not None:
        os.environ[var1] = d1
    if d2 is not None:
        os.environ[var2] = d2


def test_config_get_cache_dir():
    var1 = "MODULECMD_CACHE_DIR"
    var2 = "MODULECMD_CONFIG_DIR"
    var3 = "XDG_CACHE_HOME"

    d1 = os.environ.pop(var1, None)
    d2 = os.environ.pop(var2, None)
    d3 = os.environ.pop(var3, None)

    assert config.get_cache_dir() == os.path.expanduser("~/.cache/modulecmd")

    os.environ[var1] = "~/a/path"
    assert config.get_cache_dir() == os.path.expanduser("~/a/path")
    os.environ.pop(var1)

    os.environ[var2] = "~/b/path"
    assert config.get_cache_dir() == os.path.expanduser("~/b/path")
    os.environ.pop(var2)

    os.environ[var3] = "~/c/path"
    assert config.get_cache_dir() == os.path.expanduser("~/c/path")
    os.environ.pop(var3)

    if d1 is not None:
        os.environ[var1] = d1
    if d2 is not None:
        os.environ[var2] = d2
    if d3 is not None:
        os.environ[var3] = d3


def test_config_convert():
    assert config.convert("a,b,c", list) == ["a", "b", "c"]
    assert config.convert("0", bool) is False
    assert config.convert("1", bool) is True
    assert config.convert("1", int) == 1
    assert config.convert("1", str) == "1"


def test_config_prettystr():
    save = config._config
    config._config = config.configuration()
    config.set("serialize_chunk_size", None, "cmd_line")
    file = StringIO()
    config.prettystr(file=file)
    config.prettystr()
    config._config = save


def test_config_paths():
    config.config_file() == config._config.config_file
    config.config_dir() == config._config.config_dir


def test_config_settings(tmpdir):
    d1 = os.environ.pop("MODULECMD_CONFIG_DIR", None)
    os.environ["MODULECMD_CONFIG_DIR"] = tmpdir.strpath
    f = tmpdir.join("config.ini")
    f.write("[modulecmd]\nserialize_chunk_size = 123\nuse_tkinter = false\n")
    dbg = os.environ.pop("MODULECMD_DEBUG", None)
    os.environ["MODULECMD_DEBUG"] = "1"
    cfg = config.configuration()
    assert cfg.get("serialize_chunk_size")[1] == 123
    assert cfg.get("debug")[1] is True

    os.environ.pop("MODULECMD_CONFIG_DIR")
    if d1:
        os.environ["MODULECMD_CONFIG_DIR"] = d1

    os.environ.pop("MODULECMD_DEBUG")
    if dbg:
        os.environ["MODULECMD_DEBUG"] = dbg
