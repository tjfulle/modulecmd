import pytest
import modulecmd.xio as xio


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_module_formatted_text(tmpdir, environ, shell):
    tmpdir.mkdir("a")
    tmpdir.join("foo.py").write(
        """\
# module: enable_if = True
"""
    )
    a = tmpdir.join("a")
    a.join("1.0.py").write(
        """\
variant('a', default=True)
variant('b')
help('The help for a')
whatis('Whatis a')
"""
    )
    a.join("2.0.py").write(
        """\
variant('a', default=True)
variant('b')
help('The help for a')
whatis('Whatis a')
"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        m = env.load_module("a/1.0", ["+a", "b=baz"])
        assert m.display_name() == "a/1.0 +a b=baz"
        s = m.format_help()
        s = m.format_whatis()
        s = m.format_info()
        assert xio.colorize("%(b)sVersion%(e)s: 1.0") in s
        m = env.load_module("a")
        s = m.format_info()
        assert xio.colorize("%(b)sVersions%(e)s: 1.0, 2.0") in s
        m = env.load_module("foo")
        s = m.format_info()
        assert env.is_loaded("foo") is True
        assert xio.colorize("%(b)sVersion%(e)s: none") in s


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_module_formatted_help(tmpdir, environ, shell):
    tmpdir.mkdir("a")
    tmpdir.join("foo.py").write(
        """\
# module: enable_if = True
"""
    )
    a = tmpdir.join("a")
    a.join("1.0.py").write(
        """\
variant('a', default=True)
variant('b')
help('The help for a')
whatis('Whatis a')
"""
    )
    a.join("2.0.py").write(
        """\
variant('a', default=True)
variant('b')
help('The help for a')
whatis('Whatis a')
"""
    )
    with environ(tmpdir.strpath, mode="r+", shell=shell) as env:
        env.format_module_help("a")
        env.format_module_whatis("a")
        env.format_module_info("a")
        env.show_module("a")
        assert env.is_loaded("foo") is False


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_module_reload(tmpdir, environ, shell):
    tmpdir.mkdir("a")
    a = tmpdir.join("a")
    a.join("1.0.py").write(
        """\
variant('a', default=True)
variant('b')
help('The help for a')
whatis('Whatis a')
"""
    )
    with environ(tmpdir.strpath, mode="w", shell=shell) as env:
        env.load_module("a")
        m1 = env.find_loaded_module("a")
        env.reload_module("a")
        m2 = env.find_loaded_module("a")
        assert m1 == m2
