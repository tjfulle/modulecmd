import os
import sys
import modulecmd.util as ut


def test_util_join():
    assert ut.join(None) == ""
    assert ut.join([" ", " "]) == "   "
    assert ut.join(["a", "b"], sep="%") == "a%b"


def test_util_serialize():
    s = "abcdefghijklmnopqrstuvwxyz" * 10
    x = ut.serialize(s)
    assert ut.deserialize(x) == s
    x = ut.serialize(s, chunk_size=10)
    assert ut.deserialize(x) == s
    x = ut.serialize(s, chunk_size=-1)
    assert ut.deserialize(x) == s


def test_util_envbool():
    key = "______SPAM"
    orig = os.environ.pop(key, None)
    assert ut.envbool(key) is False
    for val in ("0", "false", "no", "off", ""):
        os.environ[key] = val
        assert ut.envbool(key) is False
    os.environ[key] = "1"
    assert ut.envbool(key) is True
    os.environ.pop(key)
    if orig is not None:
        os.environ[key] = orig


def test_util_eval_bool_expr():
    assert ut.eval_bool_expr("True") is True
    assert ut.eval_bool_expr("False") is False
    assert ut.eval_bool_expr(f"sys.platform == {sys.platform!r}") is True
    assert ut.eval_bool_expr("s.platform == 'baz'") is None


def test_util_singleton():
    class foo:
        def __init__(self):
            self.baz = 1
            self.spam = 2

        def __getitem__(self, arg):
            return 3

        def __contains__(self, arg):
            return True if arg == "xx" else False

        def __call__(self, *args):
            return list(args)

        def __iter__(self):
            for i in range(3):
                yield i

        def __str__(self):
            return "is a foo"

    x = ut.singleton(foo)
    assert x.baz == 1
    assert x.spam == 2
    assert x["foo"] == 3
    assert "xx" in x
    assert "yy" not in x
    assert x(3, 4, 5) == [3, 4, 5]
    assert list(iter(x)) == list(range(3))
    assert str(x) == "is a foo"
