import os
import pytest
import modulecmd.xio as xio
import modulecmd.filesystem as fs


def test_isreadable(tmpdir):
    foo = tmpdir.join("foo")
    foo.write("Is a file")
    assert fs.isreadable(foo.strpath, os.path.isfile)


def test_empty(tmpdir):
    foo = tmpdir.join("foo")
    foo.write("")
    assert fs.isempty(foo.strpath)
    with pytest.raises(OSError):
        fs.isempty("wubble")


def test_ispython(tmpdir):
    foo = tmpdir.join("foo.py")
    foo.write("")
    assert fs.ispython(foo.strpath)


def test_ishidden(tmpdir):
    foo = tmpdir.join(".foo")
    foo.write("")
    assert fs.ishidden(foo.strpath)


def test_working_dir(tmpdir):
    foo = tmpdir.join("foo")
    fs.mkdirp(foo.strpath)
    orig_cwd = os.getcwd()
    with fs.working_dir(foo.strpath):
        assert os.getcwd() == foo.strpath
    assert os.getcwd() == orig_cwd


def test_mkdirp(tmpdir):
    foo = tmpdir.join("foo")
    fs.mkdirp(foo.strpath)
    assert os.path.exists(foo.strpath)

    spam = tmpdir.join("spam")
    fs.mkdirp(spam.strpath, mode=0o755)
    assert os.path.exists(spam.strpath)

    baz = tmpdir.join("baz")
    baz.write("")
    with pytest.raises(OSError):
        fs.mkdirp(baz.strpath)


def which(*args, path=None, required=False):
    """Finds an executable in the path like command-line which.

    If given multiple executables, returns the first one that is found.
    If no executables are found, returns None.

    Parameters
    ----------
    args : (str)
        One or more executables to search for
    path : list or str
        The path to search. Defaults to ``PATH`` required (bool): If set to
        True, raise an error if executable not found

    Returns
    -------
    exe : str
        The first executable that is found in the path

    """
    if path is not None:
        if isinstance(path, (list, tuple)):
            path = os.pathsep.join(path)
    else:
        path = os.getenv("PATH") or []

    if isinstance(path, str):
        path = path.split(os.pathsep)

    for name in args:
        exe = os.path.abspath(name)
        if os.path.isfile(exe) and os.access(exe, os.X_OK):
            return exe
        for directory in path:
            exe = os.path.join(directory, name)
            if os.path.isfile(exe) and os.access(exe, os.X_OK):
                return exe

    if required:
        xio.die(f"{args[0]}: program not found")

    return None
