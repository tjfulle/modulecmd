import os
import pytest
import modulecmd.config


def create_module_dirs(tmpdir):
    one = tmpdir.mkdir("1")
    one.join("a.py").write("setenv('a', 'foo')")
    one.join("b.py").write("setenv('b', 'baz')")
    two = tmpdir.mkdir("2")
    two.join("c.py").write("setenv('c', 'spam')")
    two.join("d.py").write("variant('x', type=int)\nsetenv('d', 'wubble')")
    return f"{one.strpath}:{two.strpath}"


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_collection_default(tmpdir, environ, shell):
    path = create_module_dirs(tmpdir)
    assert not modulecmd.config.cache_dir().startswith(os.path.expanduser("~"))
    with environ(path, shell=shell) as env:
        env.load_module("a")
        env.load_module("b")
        # env.save_module_collection()
        # x = env.get_module_collection()
        # assert len(x) == 2
        # assert x[0]["fullname"] == "a"
        # assert x[1]["fullname"] == "b"


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_collection_named(tmpdir, environ, shell):
    path = create_module_dirs(tmpdir)
    with environ(path, shell=shell) as env:
        env.load_module("a")
        env.load_module("b")
        env.load_module("c")
        env.load_module("d", opts=["x=1"])
        env.save_module_collection("foo")
        x = env.get_module_collection("foo")
        assert len(x) == 4

        assert x[0]["fullname"] == "a"
        assert x[1]["fullname"] == "b"
        assert x[2]["fullname"] == "c"
        assert x[3]["fullname"] == "d"

        env.purge_loaded_modules()
        env.restore_module_collection("foo")
        assert len(env.loaded_modules) == 4
        for (i, char) in enumerate("abcd"):
            assert env.loaded_modules[i].name == char
        assert env.loaded_modules[-1].uopts == [["x", "1"]]
        env.format_module_collection("foo")
        env.format_available_collections()
        env.remove_module_collection("foo")
