import os
import pytest


compiler_vendor = "ucc"
mpi_vendor = "umpi"
compiler_versions = ("1.0", "2.0")
mpi_versions = ("1.0", "2.0")


def create_hierarchy(tmpdir):
    # Build the mpi dependent modules
    mpi_base_dir = tmpdir.mkdir("mpi")
    ucc_mpi_base_dir = mpi_base_dir.mkdir(compiler_vendor)
    for compiler_ver in compiler_versions:
        ucc_ver_dir = ucc_mpi_base_dir.mkdir(compiler_ver)
        umpi_base_dir = ucc_ver_dir.mkdir(mpi_vendor)
        for mpi_ver in mpi_versions:
            umpi_ver_dir = umpi_base_dir.mkdir(mpi_ver)
            b = umpi_ver_dir.mkdir("b")
            b.join("1.0.py").write("setenv('b', '1.0')")
    # Build the compiler modules that unlock compiler dependent modules
    compiler = tmpdir.mkdir("compiler")
    ucc_compiler_base_dir = compiler.mkdir(compiler_vendor)
    for compiler_ver in compiler_versions:
        ucc_ver_dir = ucc_compiler_base_dir.mkdir(compiler_ver)
        a = ucc_ver_dir.mkdir("a")
        a.join("1.0.py").write("setenv('a', '1.0')")

        umpi_base_dir = ucc_ver_dir.mkdir(mpi_vendor)
        for mpi_ver in mpi_versions:
            d = os.path.join(
                mpi_base_dir.strpath, compiler_vendor, compiler_ver, mpi_vendor, mpi_ver
            )
            f = umpi_base_dir.join(mpi_ver + ".py")
            f.write(f"setenv({mpi_vendor!r}, {mpi_ver!r})\n")
            f.write(f"use({d!r})\n")
    # Build the core modules
    core = tmpdir.mkdir("core")
    ucc_core_base_dir = core.mkdir(compiler_vendor)
    for compiler_ver in compiler_versions:
        d = os.path.join(compiler.strpath, compiler_vendor, compiler_ver)
        assert os.path.exists(d)
        f = ucc_core_base_dir.join(compiler_ver + ".py")
        f.write("family('compiler')")
        f.write(f"use({d!r})")
    return core.strpath


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_hierarchy_fixture_layout(tmpdir, environ, shell):
    """Loop through the module hierarchy to make sure it is laid out
    correctly"""
    core_path = create_hierarchy(tmpdir)
    with environ(core_path, shell=shell) as env:
        assert not env.loaded_modules
        for compiler_ver in compiler_versions:
            compiler_module_name = os.path.sep.join((compiler_vendor, compiler_ver))
            compiler = env.load_module(compiler_module_name)
            assert compiler is not None
            dirname = os.path.normpath(
                os.path.join(core_path, "..", "compiler", compiler_vendor, compiler_ver)
            )
            assert os.path.isdir(dirname)
            assert dirname in env.modulepath
            a = env.find_module("a")
            assert a is not None
            assert a.version.string == "1.0"
            assert a.file == os.path.join(dirname, a.name, a.version.string + ".py")
            for mpi_ver in mpi_versions:
                mpi_module_name = os.path.sep.join((mpi_vendor, mpi_ver))
                mpi = env.load_module(mpi_module_name)
                assert mpi is not None
                mpi_unlocks_dir = os.path.normpath(
                    os.path.join(
                        core_path,
                        "..",
                        "mpi",
                        compiler_vendor,
                        compiler_ver,
                        mpi_vendor,
                        mpi_ver,
                    )
                )
                assert os.path.isdir(mpi_unlocks_dir)
                assert mpi_unlocks_dir in env.modulepath
                env.load_module("b")


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_hierarchy_core_compiler_mpi(tmpdir, environ, shell):
    """Tests the basic functionality of module hierarchy.

    Steps:
    - Load a compiler.  The compiler unlocks compiler dependent modules
    - Load a compiler dependent module.
    - Load an mpi implementation. The mpi implementation unlocks mpi
      implementation dependent modules
    - Load an mpi implementation dependent module

    Now is the fun part
    - Load a different compiler.

    The compiler dependent and mpi dependent modules will all be updated
    accordingly

    """
    core_path = create_hierarchy(tmpdir)
    with environ(core_path, shell=shell) as env:
        _compiler_unlocks_dir = lambda cc, cv: os.path.normpath(
            os.path.join(core_path, "..", "compiler", cc, cv)
        )
        _mpi_unlocks_dir = lambda cc, cv, mpi, mpiv: os.path.normpath(
            os.path.join(core_path, "..", "mpi", cc, cv, mpi, mpiv)
        )

        compiler_ver = compiler_versions[0]
        compiler_module_name = os.path.sep.join((compiler_vendor, compiler_ver))
        compiler = env.load_module(compiler_module_name)
        assert compiler is not None

        compiler_unlocks_dir = _compiler_unlocks_dir(compiler_vendor, compiler_ver)

        assert os.path.isdir(compiler_unlocks_dir)
        assert compiler_unlocks_dir in env.modulepath

        a = env.find_module("a")
        assert a is not None
        assert a.version.string == "1.0"
        assert a.file == os.path.join(
            compiler_unlocks_dir, a.name, a.version.string + ".py"
        )

        mpi_ver = mpi_versions[0]
        mpi_module_name = os.path.sep.join((mpi_vendor, mpi_ver))
        mpi = env.load_module(mpi_module_name)
        assert mpi is not None
        assert mpi.acquired_as == mpi.fullname

        mpi_unlocks_dir = _mpi_unlocks_dir(
            compiler_vendor, compiler_ver, mpi_vendor, mpi_ver
        )

        assert os.path.isdir(mpi_unlocks_dir)
        assert mpi_unlocks_dir in env.modulepath

        b = env.load_module("b")
        assert b.file == os.path.join(mpi_unlocks_dir, b.name, b.version.string + ".py")

        # Now, load a different compiler and a, mpi, and b modules will all be
        # updated automatically
        compiler_ver = compiler_versions[1]
        compiler_module_name = os.path.sep.join((compiler_vendor, compiler_ver))
        assert mpi.acquired_as == mpi.fullname
        compiler = env.load_module(compiler_module_name)
        assert compiler is not None
        compiler_unlocks_dir = _compiler_unlocks_dir(compiler_vendor, compiler_ver)
        assert os.path.isdir(compiler_unlocks_dir)
        assert compiler_unlocks_dir in env.modulepath

        a = env.find_module("a")
        assert a.file == os.path.join(
            compiler_unlocks_dir, a.name, a.version.string + ".py"
        )

        mpi = env.find_module(mpi_module_name)
        assert mpi is not None
        mpi_unlocks_dir = _mpi_unlocks_dir(
            compiler_vendor, compiler_ver, mpi_vendor, mpi_ver
        )
        assert os.path.isdir(mpi_unlocks_dir)
        assert mpi_unlocks_dir in env.modulepath

        b = env.find_module("b")
        assert b.file == os.path.join(mpi_unlocks_dir, b.name, b.version.string + ".py")

        unlocked_by = env.unlocked_by(b)
        assert len(unlocked_by) == 2
        assert unlocked_by[0] == env.find_module("ucc/2.0")
        assert unlocked_by[1] == env.find_module("umpi/1.0")
