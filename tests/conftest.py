import os
import sys
import pytest

major, minor, patch = sys.version_info[:3]
if (major, minor) < (3, 6):
    raise SystemExit("Modulecmd.py require Python >= 3.6")
this_file = os.path.realpath(__file__)
this_dir = os.path.dirname(this_file)
prefix = os.path.dirname(this_dir)
pkg_path = os.path.join(prefix, "modulecmd")
assert os.path.isdir(pkg_path)
os.environ["modulecmd"] = pkg_path
sys.path.insert(0, prefix)


@pytest.fixture(scope="function", autouse=True)
def cleanslate(tmpdir):
    save_environ = dict(os.environ)
    import modulecmd.config
    import modulecmd.environ

    pop = ("__MODULECMD_DATA", "__MODULECMD_INI", "MODULEPATH", "MODULECMD")
    for key in list(os.environ.keys()):
        if key.startswith(pop):
            os.environ.pop(key)
    os.environ["MODULECMD_CONFIG_DIR"] = tmpdir.strpath
    os.environ["MODULECMD_CACHE_DIR"] = tmpdir.strpath
    save_config = modulecmd.config._config
    modulecmd.config._config = modulecmd.config.configuration()
    yield
    modulecmd.config._config = save_config
    names = set(list(os.environ.keys()) + list(save_environ.keys()))
    for name in names:
        if name not in save_environ:
            os.environ.pop(name, None)
            if modulecmd.environ.global_tcl is not None:
                modulecmd.environ.global_tcl.call("catch", f"unset env({name})", "")
        else:
            os.environ[name] = save_environ[name]
            if modulecmd.environ.global_tcl is not None:
                modulecmd.environ.global_tcl.call("set", f"env({name})", save_environ[name])
    modulecmd.environ.global_tcl = None


@pytest.fixture
def environ():
    import modulecmd.environ

    yield modulecmd.environ.environ
