import os
import pytest

import modulecmd.xio as xio
from modulecmd.environ import ModuleNotFoundError


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_load(tmpdir, environ, shell):
    d = tmpdir.mkdir("baz")
    for char in "abcd":
        m = d.mkdir(char)
        for v in "123":
            m.join(f"{v}.0.py").write(f"setenv({char!r}, 'FOO_{char}_{v}')")
    with environ(d.strpath, shell=shell) as env:
        m = env.load_module("a")
        assert str(m.version) == "3.0"
        m = env.load_module("d/2.0")
        assert str(m.version) == "2.0"
        m = env.load_first_module("foo", "baz", "c/1.0", "a")
        assert m.name == "c"
        assert str(m.version) == "1.0"
        with pytest.raises(ModuleNotFoundError):
            env.load_module("spam")
        m = env.unload_module("c")
        assert m.name == "c"
        assert str(m.version) == "1.0"
        with pytest.raises(ValueError):
            env.unload_module("foo")


def private_var(s):
    if s.startswith(("__", "setenv __", "os.environ['__")):
        return True
    return False


@pytest.mark.parametrize("shell", ["bash", "csh", "python"])
def test_environ_dump(tmpdir, capfd, environ, shell):
    d = tmpdir.mkdir("baz")
    for char in "abcd":
        m = d.mkdir(char)
        for v in "123":
            m.join(f"{v}.0.py").write(f"setenv({char!r}, 'FOO_{char}_{v}')")
    m = d.mkdir("e")
    m.join("4.0.py").write(
        """\
setenv('e', 'foo_4')
set_alias('x', 'baz')
set_shell_function('fn', 'foo')
unset_alias('ll')
unset_shell_function('f2')
raw('ls -l')
"""
    )
    with environ(d.strpath, shell=shell, mode="w") as env:
        env.uinit()
        env.uinit()  # call twice to make sure it skips second init
        m = env.load_module("a")
        m = env.load_module("d/2.0")
        m = env.load_first_module("foo", "baz", "c/1.0", "a")
        m = env.unload_module("c")
        env.load_module("e")
    captured = capfd.readouterr()
    out = [
        _.strip() for _ in captured.out.split("\n") if _.split() and not private_var(_)
    ]
    lm = "a/3.0:d/2.0:e/4.0"
    lm_files = f"{d.strpath}/a/3.0.py:{d.strpath}/d/2.0.py:{d.strpath}/e/4.0.py"
    mp = d.strpath
    if shell == "bash":
        assert out == [
            'a="FOO_a_3"; export a;',
            'd="FOO_d_2"; export d;',
            "unset c;",
            'e="foo_4"; export e;',
            f'LOADEDMODULES="{lm}"; export LOADEDMODULES;',
            f'_LMFILES_="{lm_files}"; export _LMFILES_;',  # noqa: E501
            f'MODULEPATH="{mp}"; export MODULEPATH;',
            "alias x='baz';",
            "unalias ll 2> /dev/null || true;",
            "fn() { foo; };",
            "unset -f f2 2> /dev/null || true;",
            "ls -l;",
        ]
    elif shell == "csh":
        assert out == [
            'setenv a "FOO_a_3";',
            'setenv d "FOO_d_2";',
            "unsetenv c;",
            'setenv e "foo_4";',
            f'setenv LOADEDMODULES "{lm}";',
            f'setenv _LMFILES_ "{lm_files}";',
            f'setenv MODULEPATH "{mp}";',
            "alias x 'baz';",
            "unalias ll 2> /dev/null || true;",
            "alias fn 'foo';",
            "unalias f2 2> /dev/null || true;",
            "ls -l;",
        ]
    elif shell == "python":
        assert out == [
            "os.environ['a'] = 'FOO_a_3'",
            "os.environ['d'] = 'FOO_d_2'",
            "del os.environ['c']",
            "os.environ['e'] = 'foo_4'",
            f"os.environ['LOADEDMODULES'] = {lm!r}",
            f"os.environ['_LMFILES_'] = {lm_files!r}",
            f"os.environ['MODULEPATH'] = {mp!r}",
            "alias_x = 'baz'",
            "alias_ll = None",
            "shell_function_fn = 'foo'",
            "shell_function_f2 = None",
        ]


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_coverage(tmpdir, environ, shell):
    os.environ["UNSETME"] = "1"
    os.environ["XYZ"] = "ABC"
    tmpdir.mkdir("core")
    tmpdir.mkdir("other")
    tmpdir.mkdir("baz")
    baz = tmpdir.join("baz")
    core = tmpdir.join("core")
    core.join("c.py").write(
        """\
setenv('FOO', 'BAZ')
setenv('P1', 'A')
setenv('P2', 'M')
setenv('P3', 'Q:R:S')
"""
    )
    core.join("e.py").write("setenv('SPAM', 'EGGS')")
    core.join("i.py").write("setenv('MI', '1')")
    core.join("j.py").write("setenv('MI', '2')")
    core.join("k.py").write("setenv('MK', '3')")
    core.join("m.py").write("setenv('LJ', '4')")
    core.join("n.py").write("setenv('LJ', '5')")
    core.join("f.py").write(
        f"""\
import os
prereq('c')
prereq_any('c')
assert getenv('FOO') == 'BAZ'
mkdirp({tmpdir.strpath!r})
assert os.path.isdir({tmpdir.strpath!r})
add_property('some_property', 'some_value')
remove_property('some_other_property')
append_path('P1', 'B', sep='%')
conflict('b')
execute('ls -l')
family('mpi')
load('e')
prepend_path('P2', 'A')
pushenv('XYZ', 'PZQ')
raw('cat "content" > {tmpdir.strpath}/foo.txt')
remove_path('P3', 'S')
set_alias('alias_a', 'a_value')
setenv('EGGS', 'HAM')
set_shell_function('fn', 'fn_value')
swap('i', 'j')
try_load('x')
unload('k')
assert 'UNSETME' in os.environ
unsetenv('UNSETME')
unset_alias('ll')
unset_shell_function('some_fun')
setenv('LD_LIBRARY_PATH', 'some_value')
unuse('{tmpdir.strpath}/other')
use('{tmpdir.strpath}/baz')
"""
    )
    p = f"{core.strpath}:{baz.strpath}"
    with environ(p, mode="w", shell=shell) as env:
        env.load_module("c")
        env.load_module("i")
        env.load_module("k")
        env.load_module("e")
        env.load_module("f")
        env.change_directory("some_directory")
        env.load_module("m")
        env.format_avail()
        env.format_avail(terse=True)
        assert os.environ["LJ"] == "4"
        env.swap_modules("m", "n")
        assert os.environ["LJ"] == "5"
        env.format_loaded()
        env.initialization_data()
    assert os.environ["P1"] == "A%B"
    assert os.environ["P2"] == "A:M"
    assert os.environ["P3"] == "Q:R"
    assert os.environ["XYZ"] == "PZQ"
    assert [m.name for m in env.loaded_modules] == ["c", "e", "j", "f", "n"]


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_load_unload(tmpdir, environ, shell):
    tmpdir.mkdir("core")
    core = tmpdir.join("core")
    os.environ["P1"] = "B:C:D"
    os.environ["P2"] = "A:B:C"
    core.join("f.py").write(
        """\
prepend_path('P1', 'A')
append_path('P1', 'E')
remove_path('P2', 'C')
"""
    )
    p = f"{core.strpath}:{core.strpath}"
    with environ(p, mode="w", shell=shell) as env:
        env.load_module("f")
        assert os.environ["P1"] == "A:B:C:D:E"
        assert os.environ["P2"] == "A:B"
        env.format_loaded()
        env.unload_module("f")
        assert os.environ["P1"] == "B:C:D"
        assert os.environ["P2"] == "A:B:C"


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_swapped_on_mp_update(tmpdir, environ, shell):
    tmpdir.mkdir("d1")
    d1 = tmpdir.join("d1")
    d1.mkdir("a")
    a = d1.join("a")
    a.join("1.0.py").write("")
    tmpdir.mkdir("d2")
    d2 = tmpdir.join("d2")
    d2.mkdir("a")
    a = d2.join("a")
    a.join("1.0.py").write("")
    with environ(d1.strpath, mode="w", shell=shell) as env:
        env.load_module("a")
        m1 = env.find_loaded_module("a")
        env.use(d2.strpath)
        m2 = env.find_loaded_module("a")
        assert m1 != m2


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_unload_on_mp_change(tmpdir, environ, shell):
    tmpdir.mkdir("d1")
    d1 = tmpdir.join("d1")
    d1.mkdir("a")
    a = d1.join("a")
    a.join("1.0.py").write("")
    tmpdir.mkdir("d2")
    d2 = tmpdir.join("d2")
    d2.mkdir("b")
    b = d2.join("b")
    b.join("1.0.py").write("")
    with environ(f"{d1.strpath}:{d2.strpath}", mode="w", shell=shell) as env:
        env.load_module("a")
        env.unuse(d1.strpath)
        assert len(env.loaded_modules) == 0


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_load_family_update(tmpdir, environ, shell):
    tmpdir.mkdir("d1")
    d1 = tmpdir.join("d1")
    d1.mkdir("a")
    a = d1.join("a")
    a.join("1.0.py").write("family('a/b')")
    tmpdir.mkdir("d2")
    d2 = tmpdir.join("d2")
    d2.mkdir("b")
    b = d2.join("b")
    b.join("2.0.py").write("family('a/b')")
    with environ(f"{d1.strpath}:{d2.strpath}", mode="w", shell=shell) as env:
        env.load_module("a/1.0")
        env.load_module("b/2.0")
        assert [m.name for m in env.loaded_modules] == ["b"]


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_initialize_loaded(tmpdir, environ, shell):
    d = tmpdir.mkdir("baz")
    for char in "abcd":
        m = d.mkdir(char)
        for v in "123":
            m.join(f"{v}.0.py").write(
                f"setenv({char!r}, 'FOO_{char}_{v}')\nfamily({char!r})"
            )
    with environ(d.strpath, shell=shell, mode="w") as env:
        m = env.load_module("a")
        assert m is not None
        m = env.load_module("d/2.0")
        assert m is not None
        m = env.load_first_module("foo", "baz", "c/1.0", "a")
        assert m is not None
    with environ(d.strpath, shell=shell) as env:
        modules = env.loaded_modules
        assert len(modules) == 3
        assert len(env.data["__families__"]) == 3


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_module_cache_clear(tmpdir, environ, shell):
    d = tmpdir.mkdir("baz")
    for char in "abcd":
        m = d.mkdir(char)
        for v in "123":
            m.join(f"{v}.0.py").write(
                f"setenv({char!r}, 'FOO_{char}_{v}')\nfamily({char!r})"
            )
    with environ(d.strpath, shell=shell, mode="w") as env:
        env.modulepath.populate()  # load the cache
        assert len(env.modulepath.cache) == 1
    with environ(d.strpath, shell=shell, mode="w") as env:
        env.modulepath.populate()  # load the cache
        env.reset_module_cache()
        assert len(env.modulepath.cache) == 1
    with environ(d.strpath, shell=shell, mode="w") as env:
        env.modulepath.populate()  # load the cache
        assert len(env.modulepath.cache) == 1
        env.clear_module_cache()
        assert len(env.modulepath.cache) == 0


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_trace(tmpdir, environ, shell):
    d = tmpdir.mkdir("baz")
    for char in "abcd":
        m = d.mkdir(char)
        for v in "123":
            m.join(f"{v}.0.py").write(
                f"setenv({char!r}, 'FOO_{char}_{v}')\nfamily({char!r})"
            )
    old_level = xio.set_log_level(xio.TRACE)
    with environ(d.strpath, shell=shell, mode="w") as env:
        m = env.load_module("a")
        m = env.load_module("d/2.0")
        m = env.load_first_module("foo", "baz", "c/1.0", "a")
    xio.set_log_level(old_level)


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_cb_unload_modules(tmpdir, environ, shell):
    tmpdir.join("a.py").write("unload('baz')")
    tmpdir.join("b.py").write("unload('c')")
    tmpdir.join("c.py").write("")
    tmpdir.join("d.py").write("load('e')")
    tmpdir.join("e.py").write("")
    tmpdir.join("f.py").write("load('e')")
    with environ(tmpdir.strpath, shell=shell, mode="w") as env:
        env.load_module("a")
        env.load_module("b")
        env.unload_module("b")
        env.load_module("c")
        env.load_module("b")
    assert [m.name for m in env.loaded_modules] == ["a", "b"]
    with environ(tmpdir.strpath, shell=shell, mode="w") as env:
        env.load_module("e")
        e = env.find_loaded_module("e")
        assert e.refcount == 1
        env.load_module("d")
        e = env.find_loaded_module("e")
        assert e.refcount == 2
        env.load_module("f")
        e = env.find_loaded_module("e")
        assert e.refcount == 3
        env.unload_module("f")
        e = env.find_loaded_module("e")
        assert e.refcount == 2
        env.unload_module("d")
        e = env.find_loaded_module("e")
        assert e.refcount == 1


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_environ_cb_swap_modules(tmpdir, environ, shell):
    tmpdir.join("a.py").write("swap('b', 'c')")
    tmpdir.join("b.py").write("")
    tmpdir.join("c.py").write("")
    with environ(tmpdir.strpath, shell=shell, mode="w") as env:
        env.uinit()
        env.load_module("a")
        c = env.find_loaded_module("c")
        assert c is not None
        env.restore()
    with environ(tmpdir.strpath, shell=shell, mode="w") as env:
        env.uinit()
        env.load_module("b")
        env.load_module("a")
        b = env.find_loaded_module("b")
        assert b is None
        c = env.find_loaded_module("c")
        assert c is not None
        env.restore()
    with environ(tmpdir.strpath, shell=shell, mode="w") as env:
        env.uinit()
        env.load_module("b")
        env.load_module("c")
        env.load_module("a")
        b = env.find_loaded_module("b")
        c = env.find_loaded_module("c")
        assert b.refcount == 1
        assert c.refcount == 2
        env.restore()
