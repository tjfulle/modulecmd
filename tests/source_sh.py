import os
import pytest


@pytest.mark.parametrize("shell", ["bash"])
def test_source(tmpdir, environ, shell):
    f = tmpdir.join("foo.sh")
    os.environ["UNSETME"] = "1"
    os.environ["SPAM"] = "BLUBBER"
    os.environ["FOO"] = "BAR"
    os.environ["BAZ"] = "WUBBLE"
    tmpdir.join("f.py").write(f"source('{f.strpath}')")
    with open(f.strpath, "w") as fh:
        fh.write(
            """\
export SPAM=EGGS
export FOO=PREPENDED:$FOO
export BAZ=$BAZ:APPENDED
unset UNSETME
alias mr='wonderful'
myfn() { ls; }"""
        )
    with environ(tmpdir.strpath, shell=shell) as env:
        env.load_module("f")
    assert len(env.data["__sourced_files__"]) == 1
    assert f.strpath in env.data["__sourced_files__"]
    data = env.data["__sourced_files__"][f.strpath]
    assert data["env"][0] == ("append-path", "BAZ", "APPENDED")
    assert data["env"][1] == ("prepend-path", "FOO", "PREPENDED")
    assert data["env"][2] == ("set", "SPAM", "EGGS")
    assert data["env"][3] == ("unset", "UNSETME", "1")
    assert data["alias"] == [("set", "mr", "wonderful")]
    fn = data["function"][0]
    assert fn[0] == "set"
    assert fn[1] == "myfn"
    assert fn[2].strip() == "ls"


@pytest.mark.parametrize("shell", ["bash"])
def test_source_unsource(tmpdir, environ, shell):
    f = tmpdir.join("foo.sh")
    os.environ["UNSETME"] = "1"
    os.environ["SPAM"] = "BLUBBER"
    os.environ["FOO"] = "BAR"
    os.environ["BAZ"] = "WUBBLE"
    tmpdir.join("f.py").write(f"source('{f.strpath}')")
    with open(f.strpath, "w") as fh:
        fh.write(
            """\
export SPAM=EGGS
export FOO=PREPENDED:$FOO
export BAZ=$BAZ:APPENDED
unset UNSETME
alias mr='wonderful'
myfn() { ls; }"""
        )
    with environ(tmpdir.strpath, shell=shell) as env:
        env.load_module("f")
        env.unload_module("f")
    assert len(env.data["__sourced_files__"]) == 0
