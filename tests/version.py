from modulecmd.version import Version


def test_version():
    v1 = Version("1.0.0")
    assert v1.major == 1
    assert v1.minor == 0
    assert v1.patch == 0
    assert v1.tweak is None
    v1b = Version("1.0.0-beta")
    assert v1b.major == 1
    assert v1b.minor == 0
    assert v1b.patch == 0
    assert v1b.tweak == "beta"


def test_version_dict():
    v1 = Version("1.0.0-beta")
    d = v1.asdict()
    v1d = Version.from_dict(d)
    assert v1 == v1d


def test_version_compare():
    v2_0_0 = Version("2.0.0")
    v2_1_0 = Version("2.1.0")
    v2_1_1 = Version("2.1.1")
    v2_1_1_b = Version("2.1.1-b")
    assert v2_1_0 > v2_0_0
    assert v2_1_1 > v2_1_0
    assert v2_1_1_b > v2_1_1
    if v2_0_0 > v2_1_1_b:
        assert 0
    va = Version("1.0.0")
    vb = Version("1.0.b")
    assert vb > va
