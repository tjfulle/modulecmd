import os
import pytest
import shutil
import modulecmd.util as util
from modulecmd.modulepath import Modulepath, find_modules
from modulecmd.module import PythonModule, TCLModule


def test_modulepath_discover_root():
    mp = Modulepath("/")
    assert not bool(mp.data)
    assert find_modules("/") is None


def test_modulepath_discover_noexist():
    mp = Modulepath("/a/fake/path")
    assert not bool(mp.data)
    assert find_modules("/a/fake/path") is None


def test_modulepath_discover_skipped(tmpdir):
    tmpdir.mkdir(".git")
    assert find_modules(os.path.join(tmpdir, ".git")) is None


def test_modulepath_discover_ignore(tmpdir):
    tmpdir.mkdir("a")
    a = tmpdir.join("a")
    a.join(".moduleignore").write("")
    tmpdir.mkdir("b")
    b = tmpdir.join("b")
    b.mkdir("bb")
    b.join("1.py").write("")
    b.join("2.py").write("")
    bb = b.join("bb")
    bb.join("1.py").write("")
    bb.join("2.py").write("")
    bb.join(".moduleignore").write("")
    assert find_modules(a.strpath) is None
    assert len(find_modules(b.strpath)) == 2


def test_modulepath_find_at(tmpdir):
    tmpdir.join("a.py").write("")
    tmpdir.join("b.py").write("")
    tmpdir.join("c.py").write("")
    mp = Modulepath(tmpdir.strpath)
    m = mp.find_at("fake", lambda m: m.name == "a")
    assert m is None
    m = mp.find_at(tmpdir.strpath, lambda m: m.name == "a")
    assert m is not None
    m = mp.find_at(tmpdir.strpath, lambda m: m.name == "d")
    assert m is None


def test_modulepath_update_empty(tmpdir):
    tmpdir.join("a.py").write("")
    tmpdir.join("b.py").write("")
    tmpdir.join("c.py").write("")
    mp = Modulepath(tmpdir.strpath)
    mp._data = None
    mp.update()
    assert mp._data is not None


def test_modulepath_cache(tmpdir):
    tmpdir.join("a.py").write("")
    tmpdir.join("b.py").write("")
    tmpdir.join("c.py").write("")
    mp = Modulepath(tmpdir.strpath)
    mp.cache.dump()
    mp = Modulepath(tmpdir.strpath)
    mp.reset_cache()
    mp = Modulepath(tmpdir.strpath)
    mp.cache.clear()


def test_modulepath_discover_not_readable(tmpdir):
    a = tmpdir.join("a")
    tmpdir.mkdir("a")
    os.chmod(a.strpath, 0x333)
    assert find_modules(a.strpath) is None
    os.chmod(a.strpath, 0x777)
    shutil.rmtree(a.strpath)


def test_modulepath_discover_nomodules(tmpdir):
    mp = Modulepath(tmpdir.strpath)
    assert not bool(mp.data[tmpdir.strpath])


def test_modulepath_discover_bad_marked_default(tmpdir):
    # Won't respect module linked to default, since these are unversioned
    # modules
    tmpdir.join("a.py").write("")
    tmpdir.join("b.py").write("")
    os.symlink(tmpdir.join("b.py").strpath, os.path.join(tmpdir.strpath, "default"))
    mp = Modulepath(tmpdir.strpath)
    modules = [b for a in mp.data.values() for b in a if a]
    assert len(modules) == 2
    names = sorted([x.name for x in modules])
    assert names == ["a", "b"]


def test_modulepath_discover_nvv(tmpdir):
    a = tmpdir.mkdir("a")
    a.join("1.0.py").write("")
    a.join("2.0.py").write("")
    one = a.mkdir("1")
    one.join("1.0.py").write("")
    one.join("2.0.py").write("")
    mp = Modulepath(tmpdir.strpath)
    modules = [b for a in mp.data.values() for b in a if a]
    assert len(modules) == 4
    x = sorted([(_.name, str(_.version)) for _ in modules])
    assert x == [("a", "1.0"), ("a", "2.0"), ("a/1", "1.0"), ("a/1", "2.0")]


def test_modulepath_discover_bad_versioned_default(tmpdir):
    a = tmpdir.mkdir("a")
    f = a.join(".version")
    f.write("")
    assert util.read_tcl_default_version(f.strpath) is None


def test_modulepath_discover_versioned_default(tmpdir):
    a = tmpdir.mkdir("a")
    a.join("1.0.py").write("")
    a.join("2.0.py").write("")
    f = a.join(".version")
    f.write('set ModulesVersion "1.0"')
    b = tmpdir.mkdir("b")
    b.join("1.0.py").write("")
    b.join("2.0.py").write("")
    os.symlink(b.join("1.0.py"), b.join("default"))
    c = tmpdir.mkdir("c")
    c.join("1.0.py").write("")
    c.join("2.0.py").write("")

    mp = Modulepath(tmpdir.strpath)

    m = mp.find("a")
    assert m.version.major == 1
    assert m.version.minor == 0
    assert m.is_explicit_default is True

    m = mp.find("b")
    assert m.version.major == 1
    assert m.version.minor == 0
    assert m.is_explicit_default is True

    m = mp.find("c")
    assert m.version.major == 2
    assert m.version.minor == 0
    assert m.is_highest_version is True

    m = mp.find("c@1.0")
    assert m.version.major == 1
    assert m.version.minor == 0
    assert m.is_highest_version is False


def test_modulepath_discover_version_split(tmpdir):
    a = tmpdir.mkdir("a")
    b = a.mkdir("b")
    c = b.mkdir("c")

    a.join(".version").write("")
    c.join("1.0.py").write("")
    c.join("2.0.py").write("")

    mp = Modulepath(tmpdir.strpath)
    m = mp.find("a")
    assert m.version_base == a.strpath
    assert str(m.version) == "b/c/2.0"


basic_py_module = """\
setenv(self.name, '1')
prepend_path('PREPEND', self.version.string)
prepend_path('APPEND', self.version.string)
"""

basic_tcl_module = """\
#%Module1.0
setenv {0} 1
prepend-path PREPEND {1}
append-path APPEND {1}
"""


@pytest.fixture()
def dirtrees(tmpdir):
    one = tmpdir.mkdir("1")
    one.join("a.py").write(basic_py_module)
    one.join("b.py").write(basic_py_module)

    py = one.mkdir("py")
    py.join("1.0.0.py").write(basic_py_module)
    py.join("2.0.0.py").write(basic_py_module)
    py.join("3.0.0.py").write(basic_py_module)
    default = py.join("default")
    default.mksymlinkto(py.join("2.0.0.py"))

    tcl = one.mkdir("tcl")
    tcl.join("1.0.0").write(basic_tcl_module.format("tcl", "1.0.0"))
    tcl.join("2.0.0").write(basic_tcl_module.format("tcl", "2.0.0"))
    tcl.join("3.0.0").write(basic_tcl_module.format("tcl", "3.0.0"))
    tcl.join(".version").write('set ModulesVersion "1.0.0"')

    ucc = one.mkdir("ucc")
    ucc.join("1.0.0.py").write(basic_py_module)
    ucc.join("2.0.0.py").write(basic_py_module)

    two = tmpdir.mkdir("2")
    ucc = two.mkdir("ucc")
    ucc.join("1.0.0.py").write(basic_py_module)
    ucc.join("4.0.0.py").write(basic_py_module)

    xxx = two.mkdir("xxx")
    xxx.join("1.0.0.py").write(basic_py_module)

    # 2 defaults
    X = one.mkdir("X")
    X.join("2.0.0.py").write(basic_py_module)
    X.join("3.0.0").write(basic_tcl_module.format("X", "3.0.0"))
    X.join(".version").write('set ModulesVersion "3.0.0"')
    default = X.join("default")
    default.mksymlinkto(X.join("2.0.0.py"))

    return tmpdir


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_two_defaults(tmpdir, environ, shell):
    one = tmpdir.mkdir("1")
    X = one.mkdir("X")
    f2 = X.join("2.0.0.py")
    f2.write(basic_py_module)
    f3 = X.join("3.0.0")
    f3.write(basic_tcl_module.format("X", "3.0.0"))
    X.join(".version").write('set ModulesVersion "3.0.0"')
    default = X.join("default")
    default.mksymlinkto(f2)
    with environ(one.strpath, shell=shell) as env:
        x = env.find_module("X")
        assert str(x.version) == "2.0.0"
        assert isinstance(x, PythonModule)


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_bad_default(tmpdir, environ, shell):
    a = tmpdir.mkdir("a")
    a.join("3.0.0").write(basic_tcl_module.format("a", "3.0.0"))
    a.join("4.0.0").write(basic_tcl_module.format("a", "4.0.0"))
    a.join(".version").write('set ModulesVersion "5.0.0"')
    with environ(tmpdir.strpath, shell=shell) as env:
        ma = env.find_module("a")
        assert ma.version.string == "4.0.0"
        assert isinstance(ma, TCLModule)


def groupby_name(modules):
    groups = {}
    for module in modules:
        groups.setdefault(module.name, []).append(module)
    return groups


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_available_1(dirtrees, environ, shell):
    dirname = dirtrees.join("1").strpath
    with environ(dirname, shell=shell) as env:
        assert len(env.modulepath) == 1
        modules = env.modulepath.get(dirname)

        grouped = groupby_name(modules)

        a = grouped.pop("a")
        assert len(a) == 1
        assert not a[0].is_default
        assert isinstance(a[0], PythonModule)

        b = grouped.pop("b")
        assert len(b) == 1
        assert not b[0].is_default
        assert isinstance(b[0], PythonModule)

        py = grouped.pop("py")
        assert len(py) == 3
        for m in py:
            assert m.is_default if m.fullname == "py/2.0.0" else not m.is_default
            assert isinstance(m, PythonModule)

        tcl = grouped.pop("tcl")
        assert len(tcl) == 3
        for m in tcl:
            assert m.is_default if m.fullname == "tcl/1.0.0" else not m.is_default
            assert isinstance(m, TCLModule)

        ucc = grouped.pop("ucc")
        assert len(ucc) == 2
        for m in ucc:
            assert m.is_default if m.fullname == "ucc/2.0.0" else not m.is_default
            assert isinstance(m, PythonModule)


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_get(dirtrees, environ, shell):
    d1 = dirtrees.join("1").strpath
    with environ(d1, shell=shell) as env:

        module = env.find_module("a")
        assert module.fullname == "a"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("b")
        assert module.fullname == "b"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("py")
        assert module.fullname == "py/2.0.0"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("tcl")
        assert module.fullname == "tcl/1.0.0"
        assert isinstance(module, TCLModule)
        assert module.file == os.path.join(d1, module.fullname)

        module = env.find_module("ucc")
        assert module.fullname == "ucc/2.0.0"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")

        modules = env.modulepath.get(d1)
        assert len(modules) == 12

        d2 = dirtrees.join("2").strpath
        env.use(d2)

        # should grab d2, since it is higher in priority
        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d2, module.fullname + ".py")

        # use more of d1, to get its module
        f = os.path.join(d1, "ucc/1.0.0")[-15:]
        module = env.find_module(f)
        assert module.fullname == "ucc/1.0.0"
        assert isinstance(module, PythonModule)
        assert module.file == os.path.join(d1, module.fullname + ".py")


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_append_path(dirtrees, environ, shell):

    d1 = dirtrees.join("1").strpath
    with environ(d1, shell=shell) as env:

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("ucc")
        assert module.fullname == "ucc/2.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        d2 = dirtrees.join("2").strpath
        d2_modules = env.use(d2, append=True)
        assert len(d2_modules) != 0
        x = env.use(d2, append=True)
        assert x is None  # d2 already on modulepath and we are appending

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("ucc")
        assert module.fullname == "ucc/4.0.0"
        assert module.file == os.path.join(d2, module.fullname + ".py")

        env.find_module("xxx")

        removed = env.unuse(d2)
        assert len(removed) == 3
        removed_full_names = [m.fullname for m in removed]
        assert "ucc/1.0.0" in removed_full_names
        assert "ucc/4.0.0" in removed_full_names
        assert "xxx/1.0.0" in removed_full_names

        module = env.find_module("ucc")
        assert module.fullname == "ucc/2.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        # No modules
        dirtrees.mkdir("FOO")
        x = env.use(dirtrees.join("FOO").strpath)
        assert x is None


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_prepend_path(dirtrees, environ, shell):
    d1 = dirtrees.join("1").strpath
    with environ(d1, shell=shell) as env:

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("ucc")
        assert module.fullname == "ucc/2.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        d2 = dirtrees.join("2").strpath
        env.use(d2)

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d2, module.fullname + ".py")

        module = env.find_module("ucc")
        assert module.fullname == "ucc/4.0.0"
        assert module.file == os.path.join(d2, module.fullname + ".py")

        removed = env.unuse(d2)
        assert len(removed) == 3
        removed_full_names = [m.fullname for m in removed]
        assert "ucc/1.0.0" in removed_full_names
        assert "ucc/4.0.0" in removed_full_names
        assert "xxx/1.0.0" in removed_full_names

        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        module = env.find_module("ucc")
        assert module.fullname == "ucc/2.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        env.use(d1)
        module = env.find_module("ucc/1.0.0")
        assert module.fullname == "ucc/1.0.0"
        assert module.file == os.path.join(d1, module.fullname + ".py")

        dirtrees.mkdir("FOO")
        a = env.use(dirtrees.join("FOO").strpath)
        assert a is None


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_walk(dirtrees, environ, shell):

    d1 = dirtrees.join("1").strpath
    d2 = dirtrees.join("2").strpath
    with environ(f"{d1}:{d2}", shell=shell) as env:
        i = 0
        for (path, modules) in env.modulepath.traverse():
            if i == 0:
                assert path == d1
                i += 1
            elif i == 1:
                assert path == d2
                i += 1
            else:
                assert False, "Should never get here"


@pytest.mark.parametrize("shell", ["bash", "csh"])
def test_modulepath_dirname_does_not_exist(tmpdir, environ, shell):
    tmpdir.join("a.py").write("")
    with environ(tmpdir.strpath, shell=shell) as env:
        modules = env.use("A/FAKE/PATH", append=True)
        assert modules is None
        bumped = env.use("A/FAKE/PATH")
        assert bumped is None
