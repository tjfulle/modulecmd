from io import StringIO
from argparse import Namespace
from modulecmd.shell import csh


def test_shell_csh_truncate():
    shell = csh()
    shell.limit = 45
    path = ":".join(["a" * 20, "b" * 20, "c" * 20])
    variables = {"PATH": path, "VAR1": "AAAAA" * 500}
    env_mods = Namespace(
        variables=variables, aliases={}, shell_functions={}, raw_shell_commands=[]
    )
    file = StringIO()
    shell.format_env_mods(env_mods, file=file)
    s = [_.strip() for _ in file.getvalue().split("\n") if _.split()]
    assert s[0] == 'setenv PATH "aaaaaaaaaaaaaaaaaaaa:/usr/bin:/bin";'
    assert s[1] == 'setenv VAR1 "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";'
