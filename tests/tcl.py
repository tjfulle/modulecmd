import os
import pytest

try:
    import tkinter  # noqa: F401

    use_tkinter = [True, False]
except ImportError:
    use_tkinter = [False]

import modulecmd.config
from modulecmd.module import TCLModule, TCLModuleError


@pytest.fixture
def tcl_module_path(tmpdir):

    d = tmpdir.mkdir("tcl2")
    p = d.join("1.0")
    p.write(
        """\
#%Module1.0
proc ModulesHelp { } {
        global dotversion

        puts stderr "\tAdds `.' to your PATH environment variable"
        puts stderr "\n\tThis makes it easy to add the current working directory"
        puts stderr "\tto your PATH environment variable.  This allows you to"
        puts stderr "\trun executables in your current working directory"
        puts stderr "\twithout prepending ./ to the excutable name"
        puts stderr "\n\tVersion $dotversion\n"
}

module-whatis   "adds `.' to your PATH environment variable"

# for Tcl script use only
set     dotversion      3.2.10

append-path     path /b/path
prepend-path    path /a/path
setenv foo BAR
set-alias alias BAZ
"""
    )

    tcl = tmpdir.mkdir("tcl")
    p = tcl.join("1.0")
    p.write(
        """\
#%Module1.0
append-path     path /b/path
prepend-path    path /a/path
setenv ENVAR tcl/1.0
set-alias foo BAR"""
    )

    a = tmpdir.mkdir("a")
    p = a.join("1.0")
    p.write(
        """\
#%Module1.0
append-path path  /c/path
setenv ENVAR a/1.0
set-alias foo BAZ"""
    )

    p = a.join("2.0")
    p.write(
        """\
#%Module1.0
append-path path  /d/path
setenv ENVAR a/2.0
set-alias foo SPAM"""
    )

    p = a.join(".version")
    p.write('set ModulesVersion "1.0"')

    return tmpdir.strpath


def test_tcl_tcl2py_1(tcl_module_path, environ):
    env = environ(tcl_module_path)
    module = env.find_module("tcl2")
    assert isinstance(module, TCLModule)
    assert module.version.string == "1.0"


def test_tcl_tcl2py_2(tcl_module_path, environ):
    env = environ(tcl_module_path)
    env.setenv("LD_LIBRARY_PATH", "path_a")
    env.setenv("LD_PRELOAD", "path_c")
    module = env.find_module("tcl2")
    assert isinstance(module, TCLModule)
    assert module.version.string == "1.0"


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_unit(tcl_module_path, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    with environ(tcl_module_path, shell=shell) as env:
        tcl = env.find_module("tcl")
        assert tcl is not None
        assert tcl.name == "tcl"
        assert tcl.fullname == "tcl/1.0"
        assert isinstance(tcl, TCLModule)

        # version 1.0 should be the default
        a1 = env.find_module("a")
        assert a1 is not None
        assert a1.name == "a"
        assert a1.fullname == "a/1.0", a1.fullname
        assert isinstance(a1, TCLModule)

        a2 = env.find_module("a/2.0")
        assert a2 is not None
        assert a2.name == "a"
        assert a2.fullname == "a/2.0"
        assert isinstance(a2, TCLModule)

        env._load_module(tcl)
        assert env.updates["__env__"]["path"] == "/a/path:/b/path"
        assert env.updates["__env__"]["ENVAR"] == "tcl/1.0"
        assert env.updates["__aliases__"]["foo"] == "BAR"

        env._load_module(a1)
        assert env.updates["__env__"]["path"] == "/a/path:/b/path:/c/path"
        assert env.updates["__env__"]["ENVAR"] == "a/1.0"
        assert env.updates["__aliases__"]["foo"] == "BAZ"

        env._load_module(a2)
        assert env.updates["__env__"]["path"] == "/a/path:/b/path:/d/path"
        assert env.updates["__env__"]["ENVAR"] == "a/2.0"
        assert env.updates["__aliases__"]["foo"] == "SPAM"


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_env_set(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    tmpdir.join("f").write(
        """\
#%Module1.0
if [info exists env(FOO)] {
  setenv BAZ $env(FOO)
} else {
  setenv BAZ SPAM
}
"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        env.setenv("FOO", "BAR")
        m = env.load_module("f")
        assert os.getenv("BAZ") == "BAR", "BAZ SHOULD BE BAR"
        assert m.is_loaded
        env.unload_module("f")

        env.unsetenv("FOO")
        m = env.load_module("f")
        assert os.getenv("BAZ") == "SPAM", os.getenv("BAZ")


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_env_break_pos(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    tmpdir.join("f").write(
        """\
#%Module1.0
if { [info exists env(FOO)] } {
  break
} else {
  setenv BAZ SPAM
}
"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        m = env.load_module("f")
        assert os.getenv("BAZ") == "SPAM"
        assert m.is_loaded
        env.unload_module("f")
        assert not m.is_loaded
        env.setenv("FOO", "BAR")
        env.setenv("BAZ", "FOO")
        assert os.environ["FOO"] == "BAR"
        m = env.load_module("f")
        assert os.getenv("BAZ") == "FOO"
        assert not m.is_loaded


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_env_break_neg(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    tmpdir.join("f").write(
        """\
#%Module1.0
if { ![info exists env(FOO)] } {
  setenv BAZ SPAM
} else {
  break
}
"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        env.unsetenv("FOO")
        env.unsetenv("BAZ")
        m = env.load_module("f")
        assert os.getenv("BAZ") == "SPAM"
        assert m.is_loaded
        env.unload_module("f")
        env.setenv("FOO", "BAR")
        env.unsetenv("BAZ")
        assert os.getenv("BAZ") is None
        assert os.getenv("FOO") == "BAR"
        m = env.load_module("f")
        assert os.getenv("BAZ") is None
        assert not m.is_loaded


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_continue(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    tmpdir.join("f").write(
        """\
#%Module1.0
setenv foo BAR
continue
setenv BAZ SPAM"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        m = env.load_module("f")
        assert os.getenv("foo") == "BAR"
        assert os.getenv("BAZ") is None
        assert m.is_loaded


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_tcl_break(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    tmpdir.join("f").write(
        """\
#%Module1.0
setenv foo BAR
break
setenv BAZ SPAM"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        m = env.load_module("f")
        assert os.getenv("foo") is None
        assert os.getenv("BAZ") is None
        assert not m.is_loaded


@pytest.mark.parametrize("shell", ["bash", "csh"])
@pytest.mark.parametrize("use_tkinter", use_tkinter)
def test_module_info(tmpdir, environ, shell, use_tkinter):
    modulecmd.config.set("use_tkinter", use_tkinter, "cmd_line")
    f = tmpdir.mkdir("f")
    f.join("1.0").write(
        """\
#%Module1.0
set name [module-info name]
if {$name != "f/1.0"} {
   error "expected name==f/1.0, got $name"
}
"""
    )
    g = tmpdir.mkdir("g")
    g.join("2.0").write(
        """\
#%Module1.0
set name [module-info name]
if {$name != "g/2.0"} {
   error "expected name==g/2.0, got $name"
}
"""
    )
    h = tmpdir.mkdir("h")
    h.join("3.0").write(
        """\
#%Module1.0
set name [module-info name]
puts stdout "the name is $name"
reportError "the module name is $name"
if {$name == "h/3.0"} {
   error "got name==h/3.0!"
}
"""
    )
    with environ(tmpdir.strpath, shell=shell) as env:
        env.load_module("f")
        env.load_module("g")
        with pytest.raises(TCLModuleError):
            env.load_module("h")
