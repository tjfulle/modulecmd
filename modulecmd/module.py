import argparse
import getpass
import os
import re
import socket
import subprocess
import sys
from io import StringIO
from typing import Any
from typing import Optional
from typing import TextIO
from typing import Union

from . import filesystem
from . import util
from . import xio
from .version import Version

builtin_type = type


class Module:
    ext = ""

    def __init__(self, root: str, path: str, _fill: bool = True) -> None:
        self.root = root
        self.path = path
        self.file = os.path.join(self.root, self.path)
        self.dirname = os.path.dirname(self.file)
        if not os.path.exists(self.file):
            raise ValueError(f"{self.file}: file not found")
        self.find_version_split()
        self.help_string: Optional[str] = None
        self.whatis_string: Optional[str] = None

        self.is_loaded: bool = False
        self.is_global_default: bool | None = None
        self.is_explicit_default: bool | None = self._is_explicit_default()
        self.is_highest_version: bool | None = self._is_highest_version()

        # command line options passed by user, used to set variants
        self.uopts: Optional[list[tuple[str, Any]]] = None
        self._variants: Optional[argparse.Namespace] = None
        self.registered_variants: dict[str, Variant] = {}

        # Attributes set externally
        self._unlocks: list[str] = []
        self.unlocked_by = None
        self.family: Optional[str] = None
        self.refcount = 0
        self.acquired_as: Optional[str] = None

    @property
    def spack_fullname(self):
        if self.version is None:
            return self.name
        return f"{self.name}@{self.version}"

    def __repr__(self) -> str:
        name = self.__class__.__name__
        return f"{name}(name={self.fullname})"

    def asdict(self) -> dict:
        kwds = {}
        for key, val in vars(self).items():
            if key == "registered_variants":
                val = [_.asdict() for _ in val.values()]
            elif key == "version":
                val = None if val is None else val.asdict()
            elif key == "_variants":
                val = None
            kwds[key] = val
        return kwds

    @classmethod
    def from_dict(cls, arg):
        self = cls.__new__(cls)
        super(Module, self).__init__()
        kwds = dict(arg)
        self.root = kwds.pop("root")
        self.path = kwds.pop("path")
        version = kwds.pop("version")
        if version is not None:
            version = Version.from_dict(version)
        self.version = version
        self._variants = None
        self.registered_variants = {}
        for vd in kwds.pop("registered_variants", []):
            variant = Variant.from_dict(vd)
            self.registered_variants[variant.name] = variant
        for key, val in kwds.items():
            setattr(self, key, val)
        return self

    @property
    def version_base(self):
        return os.path.join(self.root, self.name)

    def display_name(self):
        parts = [self.fullname]
        if self.uopts:
            self.parse_uopts()
            for name, _ in self.uopts:
                variant = self.registered_variants.get(name)
                parts.append(variant.display_name())
        return " ".join(parts)

    def endswith(self, arg: str):
        path = os.path.join(self.root, self.fullname)
        return len(arg) > len(self.fullname) and path.endswith(arg)

    def matches(self, arg: str):
        return self.name == arg or self.fullname == arg or self.endswith(arg)

    def unlocks_path(self, path: str) -> None:
        self._unlocks = self._unlocks or []
        if path not in self._unlocks:
            self._unlocks.append(path)

    def unlocks(self, path: Optional[str] = None) -> Any:
        """Return whether `path` is unlocked by this module. If `path is
        None`, then return the list of paths that are unlocked by this
        module.

        The list _unlocked_by_me is populated by the callback `use` through
        the `unlocks_path` function.
        """
        if path is None:
            return list(self._unlocks)
        return path in self._unlocks

    def find_version_split(self):
        self.fullname = self.path if not self.ext else os.path.splitext(self.path)[0]
        parts = self.fullname.split(os.path.sep)
        if len(parts) == 1:
            self.version = None
            self.name = self.fullname
        elif len(parts) == 2:
            self.name = parts[0]
            self.version = Version(parts[1])
        else:
            # The fullname is name/version.  The name can have path
            # separators in it
            version_string = parts.pop()
            while parts:
                dirname = os.path.join(self.root, *parts)
                version_file = os.path.join(dirname, ".version")
                if os.path.exists(version_file):
                    self.name = os.path.sep.join(parts)
                    self.version = Version(version_string)
                    break
                version_string = f"{parts.pop()}/{version_string}"
            else:
                self.name, version_string = os.path.split(self.fullname)
                self.version = Version(version_string)
        return

    @staticmethod
    def valid(path: str) -> bool:
        raise NotImplementedError

    @property
    def enabled(self):
        return True

    @property
    def variants(self):
        if self._variants is None:
            self._variants = self.parse_uopts()
        return self._variants

    def parse_uopts(self) -> argparse.Namespace:
        """Parse the user given options"""
        ns = argparse.Namespace()
        for variant in self.registered_variants.values():
            setattr(ns, variant.name, variant.default)
        uopts = self.uopts or []
        for name, value in uopts:
            if name not in self.registered_variants:
                raise ValueError(f"{name!r} invalid variant for {self.name}")
            variant = self.registered_variants[name]
            variant.value = value
            setattr(ns, variant.name, variant.value)
        return ns

    def set_opts(self, opts: Optional[list[Union[str, tuple[str, str]]]]) -> None:
        """Store options passed on command line"""
        self.uopts = []
        if not opts:
            return
        for opt in opts:
            if isinstance(opt, str):
                name, value = Variant.parse(opt)
            else:
                name, value = opt
            self.uopts.append((name, value))

    def add_variant(
        self,
        name: str,
        default: Optional[Any] = None,
        description: Optional[str] = None,
        type: Optional[Any] = None,
    ):
        var = Variant(name, default=default, type=type, description=description)
        self.registered_variants[var.name] = var

    def add_help(self, arg: str) -> None:
        self.help_string = arg

    def add_whatis(self, *args: str, **kwargs: dict[str, str]) -> None:
        fh = StringIO()
        for arg in args:
            fh.write(f"{arg}\n")
        for key, value in kwargs.items():
            fh.write(f"{key}: {value}\n")
        self.whatis_string = self.whatis_string or ""
        self.whatis_string += fh.getvalue()

    def format_help(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        if self.help_string is None:
            file.write(f"{self.fullname}: no help string provided\n")
        else:
            file.write(f"{self.help_string}\n")
            if self.registered_variants:
                file.write("\nmodule options:\n")
                for variant in self.registered_variants.values():
                    variant.format_help(file=file)
                    file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_whatis(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        s = self.whatis_string or f"{self.fullname}: no whatis string provided\n"
        file.write(f"{s}\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_info(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        file.write(xio.colorize(f"%(*)s{self.name}%(e)s\n"))
        file.write(xio.colorize(f"  %(b)sRoot%(e)s: {self.root}\n"))
        if self.version is None:
            file.write(xio.colorize("  %(b)sVersion%(e)s: none\n"))
        elif self.acquired_as == self.fullname:
            file.write(xio.colorize(f"  %(b)sFile%(e)s: {self.file}\n"))
            file.write(xio.colorize(f"  %(b)sVersion%(e)s: {self.version}\n"))
        else:
            versions = ", ".join(str(_) for _ in self.all_versions() or [])
            file.write(xio.colorize(f"  %(b)sVersions%(e)s: {versions}\n"))
        if self.registered_variants:
            file.write(xio.colorize("  %(b)sVariants%(e)s:\n"))
            for variant in self.registered_variants.values():
                variant.format_help(file=file, indent=4)
                file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def exec(self, mode: str, env: Any) -> None:
        raise NotImplementedError

    def _is_explicit_default(self) -> Optional[bool]:
        if self.version is None:
            return None
        basename = os.path.basename(self.file)
        link = os.path.join(self.version_base, "default")
        version_file = os.path.join(self.version_base, ".version")
        if os.path.exists(link):
            if os.path.islink(link):
                return os.path.samefile(link, self.file)
            xio.debug(f"{link} is not a symbolic link!")
        if os.path.exists(version_file):
            default_version = util.read_tcl_default_version(version_file)
            if default_version:
                f = default_version + self.ext
                default_file = os.path.join(self.version_base, f)
                if os.path.exists(default_file):
                    return f == basename
        return None

    @staticmethod
    def get_version(string):
        try:
            return Version(string)
        except ValueError:
            return None

    def all_versions(self) -> Optional[list[Version]]:
        if self.version is None:
            return None
        defaults = (".version", "default")
        members = [f for f in os.listdir(self.version_base) if f not in defaults]
        if not self.ext:
            versions = [self.get_version(_) for _ in members]
        else:
            versions = [self.get_version(os.path.splitext(_)[0]) for _ in members]
        return sorted([v for v in versions if v])

    def _is_highest_version(self) -> Optional[bool]:
        candidates = self.all_versions()
        if not candidates:
            return None
        if len(candidates) == 1:
            return False
        return candidates[-1] == self.version

    @property
    def is_default(self) -> bool:
        if self.is_global_default is not None:
            return self.is_global_default
        elif self.is_explicit_default is not None:
            return self.is_explicit_default
        elif self.is_highest_version is not None:
            return self.is_highest_version
        return False


class PythonModule(Module):
    ext = ".py"

    def __init__(self, root: str, path: str) -> None:
        super(PythonModule, self).__init__(root, path)
        self.enable_if = True
        self.read_meta_data()

    @staticmethod
    def valid(path) -> bool:
        if not filesystem.isreadable(path):
            return False
        elif not filesystem.ispython(path):
            return False
        elif filesystem.ishidden(path):
            return False
        return True

    @property
    def enabled(self) -> bool:
        return self.enable_if

    def std_callbacks(self) -> dict[str, Any]:
        return {
            "self": self,
            "help": self.add_help,
            "whatis": self.add_whatis,
            "mcontinue": raises(ContinueError),
            "mbreak": raises(BreakError),
            "variant": self.add_variant,
        }

    def compile(self):
        return compile(open(self.file).read(), self.file, "exec")

    def exec(self, mode: str, env: Any) -> None:
        globals = self.std_callbacks()
        globals.update(env.std_callbacks(mode))
        code = self.compile()
        exec(code, globals, None)

    def read_meta_data(self) -> None:
        """Reads meta data
        # module: [enable_if=<bool expr>]
        """
        kwds = {}
        for line in open(self.file):
            if not line.split():
                continue
            if not line.startswith("#"):
                break
            if line.startswith(("# module:", "# pymod:")):
                directive = line.split(":", 1)[1].strip()
                key, val = util.split(directive, "=", 1)
                kwds[key] = val
        for key in ("enable_if",):
            expr = kwds.pop(key, None)
            if expr is None:
                continue
            value = util.eval_bool_expr(expr)
            if value is None:
                msg = f"{self.fullname}: {expr} is not a boolean expression"
                raise ValueError(msg)
            setattr(self, key, value)
        if len(kwds):
            msg = f"{self.fullname}: unknown meta options: {list(kwds.keys())}"
            raise ValueError(msg)


class TCLModule(Module):
    ext = ""

    @staticmethod
    def valid(path) -> bool:
        if not os.path.isfile(path) or filesystem.ishidden(path):
            return False
        try:
            return open(path).readline().startswith("#%Module")
        except (IOError, UnicodeDecodeError):
            return False

    def std_callbacks(self) -> dict[str, Any]:
        return {
            "help": self.add_help,
            "whatis": self.add_whatis,
            "reportError": raises(TCLModuleError),
            "reportInfo": xio.info,
            "uname": os.uname,
            "gethostname": socket.gethostname,
        }

    def exec(self, mode: str, env: Any) -> None:
        from . import environ

        if environ.global_tcl is None:
            return self.exec2(mode, env)
        return self.exec1(mode, env)

    def exec2(self, mode: str, env: Any) -> None:
        globals = self.std_callbacks()
        globals.update(env.std_callbacks(mode))
        globals["mbreak"] = raises(BreakError)
        globals["mcontinue"] = raises(ContinueError)
        globals["module-whatis"] = self.add_whatis
        code = self.compile(mode, env.loaded_modules)
        exec(code, globals, None)

    def compile(self, mode: str, lm_objs: list[Module]):
        """use tcl2py to convert TCL source to python"""
        #assert mode in ("load", "unload", "display"), f"compile: incorrect mode: {mode}"
        args = [os.path.join(os.path.dirname(__file__), "tcl2py.tcl")]
        lm_names = util.unique([x for m in lm_objs for x in [m.name, m.fullname]])
        args.extend(("-l", ":".join(lm_names)))
        args.extend(("-f", self.fullname))
        args.extend(("-m", "remove" if mode == "unload" else mode))
        args.extend(("-u", self.name))
        args.extend(("-s", "bash"))
        varname = "DYLD_LIBRARY_PATH" if sys.platform == "darwin" else "LD_LIBRARY_PATH"
        ldlib = os.getenv(varname)
        if ldlib:
            args.extend(("-L", ldlib))
        ld_preload = os.getenv("LD_PRELOAD")
        if ld_preload:
            args.extend(("-P", ld_preload))
        args.append(self.file)
        out = subprocess.check_output(args, env=os.environ, encoding="utf-8")
        return compile(out, self.file, "exec")

    def add_module_info(self, tcl: Any, mode: str, shell: str) -> None:
        ns = {
            "mode": mode,
            "shell": shell,
            "user": getpass.getuser(),
            "fullname": self.fullname,
            "version": "default" if self.version is None else str(self.version),
        }
        body = """\
set mode %(mode)s
switch -- $what {
  "mode" {
  if {$more != ""} {
    if {$mode == $more} { return 1 } else { return 0 } } else { return $mode }
  }
  "shell" { return %(shell)s }
  "shelltype" { return %(shell)s }
  "flags" { return 0 }
  "name" { return %(fullname)s }
  "specified" { return %(user)s }
  "version" { return "%(version)s" }
  "alias" { return {} }
  default {
    error "module-info $what not supported"
    return {}
  }
}
"""
        tcl.call("proc", "module-info", "what {more {}}", body % ns)

    def exec1(self, mode: str, env: Any) -> None:
        import tkinter

        from . import environ

        tcl = environ.global_tcl
        assert tcl is not None
        for name, fun in self.std_callbacks().items():
            tcl.createcommand(name, fun)
        for name, fun in env.std_callbacks(mode).items():
            tcl.createcommand(name.replace("_", "-"), fun)
        tcl.createcommand("module-whatis", self.add_whatis)
        tcl.createcommand("is-loaded", lambda: self.is_loaded)
        tcl.createcommand("module-verbosity", lambda *x: None)
        tcl.createcommand("help", self.add_help)
        tcl.call("set", "ModulesCurrentModulefile", self.file)
        tcl.createcommand("", lambda: None)
        self.add_module_info(tcl, mode, env.shell.type)
        try:
            tcl.evalfile(self.file)
        except tkinter._tkinter.TclError as e:  # type: ignore
            err = e.args[0].strip().lower()
            if err.startswith('invoked "break" outside of'):
                raise BreakError from None
            elif err.startswith('invoked "continue" outside of'):
                raise ContinueError from None
            elif not err.split():
                return self.exec2(mode, env)
            raise TCLModuleError(err) from None


class Variant:
    arg_re = "^[^\d\W]\w*=.*"
    bool_arg_re = "^[+~]\w+"

    def __init__(self, name, default=None, type=None, description=None):
        self.name = name
        self.default = default
        self._value = "==UNSET=="
        self.description = description
        if default is None:
            self.type = type or str
        else:
            if type is not None:
                if builtin_type(default) != type:
                    raise ValueError(f"variant({name}): inconsistent type/default")
            self.type = builtin_type(default)

    def __repr__(self):
        s = f"Variant({self.name}"
        if self._value != "==UNSET==":
            s += f", current={self._value}"
        s += f", default={self.default})"
        return s

    def display_name(self):
        if self._value == "==UNSET==":
            return ""
        elif self.type is bool:
            return f"+{self.name}" if self._value else f"~{self.name}"
        else:
            return f"{self.name}={self._value}"

    def asdict(self):
        kwds = {}
        for key, val in vars(self).items():
            if key == "type":
                val = val.__name__
            kwds[key] = val
        return kwds

    @classmethod
    def from_dict(cls, arg):
        kwds = dict(arg)
        self = cls(
            kwds.pop("name"),
            default=kwds.pop("default"),
            type=eval(kwds.pop("type")),
            description=kwds.pop("description"),
        )
        for key, val in kwds.items():
            setattr(self, key, val)
        return self

    def format_help(self, file: Optional[TextIO] = None, indent: int = 2) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        space = " " * indent
        if self.type is bool:
            file.write(f"{space}{self.name:10s}")
        else:
            s = f"{self.name}={self.name.upper()}"
            file.write("{0}{1:10s}".format(space, s))
        if self.description:
            file.write(f"   {self.description}")
        if self.default is not None:
            file.write(f"  [default: {self.default}]")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    @staticmethod
    def validate(arg):
        if re.search(Variant.bool_arg_re, arg):
            return True
        elif re.search(Variant.arg_re, arg):
            return True
        return 0

    @staticmethod
    def parse(arg: str) -> tuple[str, Any]:
        name: str
        value: Any
        if re.search(Variant.bool_arg_re, arg):
            name = arg[1:]
            value = True if arg[0] == "+" else False
        elif re.search(Variant.arg_re, arg):
            name, value = arg.split("=", 1)
        else:
            xio.die(f"{arg!r}: invalid option name/value format")
        return name, value

    @property
    def value(self):
        return self.default if self._value == "==UNSET==" else self._value

    @value.setter
    def value(self, arg):
        if self.type is bool:
            if arg in (True, False):
                self._value = arg
            elif isinstance(arg, str):
                b = False if arg.lower() in ("", "0", "false", "no", "off") else True
                self._value = b
            else:
                self._value = bool(arg)
        else:
            self._value = self.type(arg)


def isvariant(arg):
    return Variant.validate(arg)


def isbackup_file(path):
    return path.endswith(("~", ".bak"))


def ismodule(path):
    if isbackup_file(path):
        return False
    if os.path.islink(path) and path.endswith("default"):
        return False
    if not os.path.isfile(path) or filesystem.ishidden(path):
        return False
    return PythonModule.valid(path) or TCLModule.valid(path)


def factory(root: str, path: str, check: bool = True) -> Optional[Module]:
    if check:
        if not ismodule(os.path.join(root, path)):
            return None
    if path.endswith(".py"):
        return PythonModule(root, path)
    elif os.path.isfile(os.path.join(root, path)):
        return TCLModule(root, path)
    raise ValueError(f"Unknown module type for {path}")


def from_dict(kwds):
    if kwds["path"].endswith(".py"):
        return PythonModule.from_dict(kwds)
    else:
        return TCLModule.from_dict(kwds)


class ContinueError(Exception):
    pass


class BreakError(Exception):
    pass


class TCLModuleError(Exception):
    pass


def raises(exc_type):
    def _raises(*args):
        raise exc_type(*args)

    return _raises
