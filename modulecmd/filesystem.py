import errno
import os
from contextlib import contextmanager
from typing import Callable
from typing import Generator
from typing import Optional


def ancestor(dir: str, n: int = 1) -> str:
    """Get the nth ancestor of a directory."""
    parent = os.path.abspath(dir)
    for i in range(n):
        parent = os.path.dirname(parent)
    return parent


def isreadable(path: str, validator: Callable = os.path.exists) -> bool:
    return validator(path) and os.access(path, os.R_OK)


def isempty(path: str) -> bool:
    return os.path.getsize(path) == 0


def ispython(path: str) -> bool:
    return os.path.isfile(path) and path.endswith(".py")


def ishidden(path: str) -> bool:
    return os.path.basename(path).startswith(".")


@contextmanager
def working_dir(dirname: str) -> Generator[None, None, None]:
    orig_dir = os.getcwd()
    os.chdir(dirname)
    yield
    os.chdir(orig_dir)


def mkdirp(path: str, mode: Optional[int] = None) -> None:
    """Creates a directory, as well as parent directories if needed.

    Arguments:
        path (str): path to create

    Keyword Aguments:
        mode (permission bits or None, optional): optional permissions to
            set on the created directory -- use OS default if not provided
    """
    if not os.path.exists(path):
        try:
            os.makedirs(path)
            if mode is not None:
                os.chmod(path, mode)
        except OSError as e:  # pragma: no cover
            if e.errno != errno.EEXIST or not os.path.isdir(path):
                raise e
    elif not os.path.isdir(path):
        raise OSError(errno.EEXIST, "File already exists", path)
