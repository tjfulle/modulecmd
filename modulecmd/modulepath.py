import difflib
import json
import os
from collections import OrderedDict
from typing import Callable
from typing import Generator
from typing import ItemsView
from typing import Optional

from . import config
from . import xio
from .filesystem import ishidden
from .filesystem import isreadable
from .filesystem import mkdirp
from .module import Module
from .module import factory as module_factory
from .util import unique


class Modulescache:
    def __init__(self) -> None:
        dir = config.get_cache_dir()
        assert isinstance(dir, str)
        self.file = os.path.join(dir, "modulescache.json")
        self.data: dict[str, list[str]] = self.load()
        self.modified: bool = False

    def __contains__(self, root: str) -> bool:
        return root in self.data

    def __getitem__(self, root: str) -> list[str]:
        return self.data[root]

    def __len__(self) -> int:
        return len(self.data)

    def items(self) -> ItemsView[str, list[str]]:
        return self.data.items()

    def update(self, root: str, modules: list[Module]) -> None:
        self.data[root] = [module.path for module in modules]
        self.modified = True

    def remove(self, root: str) -> None:
        self.data.pop(root, None)
        self.modified = True

    def clear(self):
        self.data.clear()
        if os.path.exists(self.file):
            os.remove(self.file)
        self.modified = True

    def load(self) -> dict[str, list[str]]:
        cache: dict[str, list[str]] = {}
        if os.path.isfile(self.file):
            data = json.load(open(self.file))
            cache.update(data["modulescache"])
        return cache

    def dump(self) -> None:
        mkdirp(os.path.dirname(self.file))
        with open(self.file, "w") as fh:
            json.dump({"modulescache": self.data}, fh, indent=2)
        self.modified = False

    def show(self) -> None:
        for root, paths in self.data.items():
            xio.print(f"{root}:")
            for path in paths:
                xio.print(f"- {path}")


class Modulepath:
    varname = "MODULEPATH"

    def __init__(self, path: Optional[str] = None, sep: Optional[str] = None):
        xio.trace("Initializing modulepath")
        path = path or os.getenv(self.varname, "")
        assert isinstance(path, str)
        paths = [_.strip() for _ in path.split(sep or os.pathsep) if _.split()]
        self.roots = list(unique([expand_name(p) for p in paths if isreadable(p, os.path.isdir)]))
        self._data: OrderedDict[str, list[Module]] | None = None
        self.cache: Modulescache = Modulescache()
        self.ignores: list[str] = self.load_ignores()
        self.defaults: dict[str, Module] = {}

    def __contains__(self, arg):
        return self._data is not None and arg in self._data

    def __len__(self):
        return len(self.roots)

    def __str__(self):
        return os.pathsep.join(self.roots)

    def clear_cache(self):
        self.cache.clear()

    def refresh_cache(self):
        self.cache.clear()
        for root, modules in self.data.items():
            self.cache.update(root, modules)
        self.cache.dump()

    def show_cache(self):
        self.cache.show()

    def reset_cache(self) -> None:
        self.cache.clear()
        self._modules = None
        self.populate()

    @property
    def ignores_file(self) -> str:
        cache_dir = config.get_cache_dir()
        assert isinstance(cache_dir, str)
        ignores_file = os.path.join(cache_dir, "modulesignore.json")
        return ignores_file

    def clear_ignores(self) -> None:
        self.ignores = []
        if os.path.exists(self.ignores_file):
            os.remove(self.ignores_file)

    def load_ignores(self) -> list[str]:
        file = self.ignores_file
        if os.path.isfile(file):
            ignores = json.load(open(file))
            return ignores["modulesignore"]
        return []

    def add_path_to_ignore(self, path) -> None:
        if path not in self.ignores:
            self.ignores.append(path)
            self.dump_ignores()

    def dump_ignores(self) -> None:
        file = self.ignores_file
        if not os.path.isfile(file):
            mkdirp(os.path.dirname(file))
        with open(file, "w") as fh:
            json.dump({"modulesignore": self.ignores}, fh, indent=2)

    @property
    def data(self) -> OrderedDict[str, list[Module]]:
        if self._data is None:
            self.populate()
        assert self._data is not None
        return self._data

    def find_modules(self, root: str) -> list[Module] | None:
        modules: list[Module] = []
        if root in self.cache:
            for p in self.cache[root]:
                if not os.path.exists(os.path.join(root, p)):
                    # cache has been invalidated, break out and re-find
                    self.cache.remove(root)
                    break
                module: Optional[Module] = module_factory(root, p, check=False)
                if module:
                    modules.append(module)
            return modules
        found = find_modules(root, ignore=self.ignores)
        if found is None:
            return None
        modules.extend(found)
        self.cache.update(root, modules)
        return modules

    def populate(self) -> None:
        xio.trace("Populating modulepath with modules")
        self._data = OrderedDict()
        for root in self.roots:
            modules = self.find_modules(root)
            if modules is None:
                xio.debug(f"No modules found in {root}")
            else:
                self._data[root] = modules
        self.assign_global_defaults()
        if self.cache.modified:
            self.cache.dump()

    def update(self) -> None:
        xio.trace("Updating modulepath")
        if self._data is None:
            self.populate()
            return
        all_modules = OrderedDict()
        for root in self.roots:
            modules = self._data.pop(root, None)
            if modules is None:
                modules = self.find_modules(root)
            if modules:
                all_modules[root] = modules
        self._data = all_modules
        self.assign_global_defaults()
        if self.cache.modified:
            self.cache.dump()

    def prepend_path(self, path: str) -> Optional[list[Module]]:
        xio.trace(f"Adding {path} to modulepath")
        path = expand_name(path)
        if not os.path.isdir(path):
            xio.warn(f"{path}: not a directory")
            return None
        if path not in self.roots:
            self.roots.insert(0, path)
        else:
            self.roots.insert(0, self.roots.pop(self.roots.index(path)))
        self.update()
        return self.data.get(path)

    def append_path(self, path: str) -> Optional[list[Module]]:
        xio.trace(f"Adding {path} to modulepath")
        if not os.path.isdir(path):
            xio.warn(f"{path}: not a directory")
            return None
        path = expand_name(path)
        if path in self.roots:
            return None
        self.roots.append(path)
        self.update()
        return self.data.get(path)

    def remove(self, path: str) -> Optional[list[Module]]:
        xio.trace(f"Removing {path!r} from modulepath")
        removed: Optional[list[Module]] = None
        if path in self.roots:
            if self._data:
                removed = self._data.pop(path, None)
            self.roots.pop(self.roots.index(path))
            self.update()
        return removed

    def find(self, name: str) -> Optional[Module]:
        xio.trace(f"Finding module {name!r}")
        best_choice: Optional[Module] = None
        if os.path.isfile(name):
            root, path = os.path.split(name)
            m = module_factory(root, path)
            if m is None:
                xio.die(f"{name}: file is not a module")
            return m
        for modules in self.data.values():
            for i, m in enumerate(modules):
                if name == m.fullname:
                    return m
                elif name == m.spack_fullname:
                    return m
                elif name == m.name:
                    if m.is_default:
                        return m
                    elif best_choice is None:
                        best_choice = m
                elif m.endswith(name):
                    return m
        return best_choice

    def find_at(self, root: str, key: Callable) -> Optional[Module]:
        modules = self.data.get(root)
        if modules is None:
            return None
        for m in modules:
            if key(m):
                return m
        return None

    def update_module(self, module: Module) -> bool:
        root = module.root
        modules = self.data.get(root)
        if modules is not None:
            for i, other in enumerate(modules):
                if other.path == module.path:
                    modules[i] = module
                    return True
        return False

    def get(self, root: str) -> Optional[list[Module]]:
        return self.data.get(root)

    def traverse(self) -> Generator[tuple, None, None]:
        for root, modules in self.data.items():
            yield (root, modules)

    def assign_global_defaults(self) -> None:
        """Assign defaults to modules.
        1. Look for an exact match in all MODULEPATH directories. Pick the
           first match.
        2. If the name doesn't contain a version, look for a marked default in
           the first directory that has one.
        3. Look for the Highest version in all MODULEPATH directories. If there
           are two or more modulefiles with the Highest version then the first one
           in MODULEPATH order will be picked.

        Given a module with multiple versions, the default is the module with
        the highest version across all modules, unless explicitly made the
        default. A module is explicitly made the default by creating a symlink
        to it (in the same directory) named 'default'
        """
        xio.trace("Assigning global module defaults")

        def sort_key(module: Module) -> tuple[int, str, int]:
            n = list(self.data.keys()).index(module.root)
            return (1 if module.is_explicit_default else -1, module.version, -n)

        self.defaults.clear()
        grouped: dict[str, list[Module]] = {}
        for modules in self.data.values():
            if not modules:
                continue
            for m in modules:
                grouped.setdefault(m.name, []).append(m)
        for name, modules in grouped.items():
            for m in modules:
                m.is_global_default = None
            if len(modules) > 1:
                modules = sorted(modules, key=sort_key, reverse=True)
                modules[0].is_global_default = True
                for module in modules[1:]:
                    module.is_global_default = False
            self.defaults[name] = modules[0]

    def candidates(self, name: str) -> list[str]:
        names: list[str] = []
        threshold: float = 0.75
        for modules in self.data.values():
            for m in modules:
                if m.name not in names and ratcliff_ratio(name, m.name) > threshold:
                    names.append(m.name)
                if m.fullname not in names and ratcliff_ratio(name, m.fullname) > threshold:
                    names.append(m.fullname)
        names.sort(key=lambda _: ratcliff_ratio(name, _), reverse=True)
        return names[:5]


def skip_dir(dirname: str, ignore: list[str] | None = None) -> bool:
    if dirname == os.path.sep:
        xio.debug("Requesting to find modules in root directory")
        return True
    elif not os.path.isdir(dirname):
        xio.debug(f"{dirname!r} is not a directory")
        return True
    elif os.path.exists(os.path.join(dirname, ".moduleignore")):
        return True
    elif ishidden(dirname):
        return True
    elif not isreadable(os.path.join(dirname, ".")):
        xio.debug(f"{dirname!r} directory is not readable")
        return True
    elif ignore and dirname in ignore:
        return True
    return False


def find_modules(root: str, ignore: list[str] | None = None) -> list[Module] | None:
    root = os.path.abspath(root)
    if skip_dir(root, ignore=ignore):
        return None
    modules: list[Module] = []
    for dirname, dirs, files in os.walk(root):
        if skip_dir(dirname, ignore=ignore):
            del dirs[:]
            continue
        for f in files:
            path = os.path.relpath(os.path.join(dirname, f), root)
            module = module_factory(root, path)
            if module is not None and module.enabled:
                modules.append(module)
    modules = sorted(modules, key=lambda m: (m.name, m.version))
    xio.trace(f"Found {len(modules)} in {root}")
    return modules


def expand_name(path: str) -> str:
    return os.path.abspath(os.path.expanduser(path))


def ratcliff_ratio(arg1: str, arg2: str) -> float:
    return difflib.SequenceMatcher(None, arg1, arg2).ratio()
