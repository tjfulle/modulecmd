from typing import Optional

from .util import safe_int
from .util import split


class Version:
    def __init__(self, version_string: Optional[str] = None) -> None:
        major = minor = patch = tweak = None
        if version_string is not None:
            version_string, *remainder = split(version_string, "-", 1)
            if remainder:
                tweak = remainder[0]
            parts = split(version_string, ".", transform=safe_int)
            major = parts[0]
            if len(parts) > 1:
                minor = parts[1]
            if len(parts) > 2:
                patch = parts[2]
        self.version_info = (major, minor, patch, tweak)

    def asdict(self):
        """Return `self` as a dictionary"""
        major, minor, patch, tweak = self.version_info
        return {"major": major, "minor": minor, "patch": patch, "tweak": tweak}

    @classmethod
    def from_dict(cls, kw):
        """The reverse of `asdict`.  Returns a Version object"""
        self = cls.__new__(cls)
        super(Version, self).__init__()
        major, minor, patch, tweak = kw["major"], kw["minor"], kw["patch"], kw["tweak"]
        self.version_info = (major, minor, patch, tweak)
        return self

    def __str__(self):
        string = ""
        string = ".".join([str(_) for _ in self.version_info[:-1] if _ is not None])
        if self.version_info[-1] is not None:
            string += f"-{self.version_info[-1]}"
        return string

    def __eq__(self, other):
        if not isinstance(other, Version):
            raise ValueError(f"Cannot compare version with {type(other).__name__}")
        return self.version_info == other.version_info

    def __lt__(self, other):
        return not self > other

    def __gt__(self, other):
        if not isinstance(other, Version):
            raise ValueError(f"Cannot compare version with {type(other).__name__}")
        for i, my_val in enumerate(self.version_info):
            other_val = other.version_info[i]
            if my_val == other_val:
                continue
            if my_val is None:
                return False
            elif other_val is None:
                return True
            elif type(my_val) == type(other_val):
                return my_val > other_val
            elif str(my_val) > str(other_val):
                return True
        return False  # pragma: no cover

    @property
    def major(self):
        return self.version_info[0]

    @property
    def minor(self):
        return self.version_info[1]

    @property
    def patch(self):
        return self.version_info[2]

    @property
    def tweak(self):
        return self.version_info[3]

    @property
    def string(self):
        return str(self)
