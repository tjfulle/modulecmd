import json
import os
import re
import shlex
import subprocess
import sys
from contextlib import contextmanager
from io import StringIO
from typing import Any
from typing import Optional
from typing import Pattern
from typing import TextIO
from typing import Union

from . import config
from . import filesystem
from . import util
from . import xio
from .module import BreakError
from .module import ContinueError
from .module import Module
from .module import from_dict as module_from_dict
from .modulepath import Modulepath
from .modulepath import expand_name
from .shell import factory as shell_factory
from .util import deserialize
from .util import serialize
from .util import unique

global_tcl = None


class environ:
    """Representation of the changing environment.  All modulefile functions
    call back to this class to modify the environment

    Parameters
    ----------
    modulepath : str
        The initial modulepath
    mode : str {r, w}
        The environment opening mode
        r: environment modifications will not persist
        w: environment modifications will persist
        if the mode ends with '+', a trace of the modulefile commands will be
        printed to the console
    shell : str
        Which shell (name) to ue

    Notes
    -----

    The environ object is intended to be used as a context manager

    >>> with environ(modulepath=modulepath, mode="r", shell="bash") as env:
    ...    env.do_something()

    If ``mode == "r"``, all output is written to stderr and, therefore, will not be
    evaluated by the shell.  If ``mode == "w"``, when the context is exited the
    modifications to the environment will be written to stdout out and
    evaluated by the shell.  If ``mode == "w+``, modifications to the envionment
    will first be printed to stderr and the use asked to confirm before printing
    to stdout - where it will be evaluated by the shell.

    """

    ini_fmt = "__MODULECMD_INI%02d__"
    meta_fmt = "__MODULECMD_DATA%02d__"
    internal_vars = ("__MODULECMD_", "_LMFILES", "LOADEDMODULES", "MODULEPATH")

    def __init__(
        self, modulepath: Optional[str] = None, mode: str = "r", shell: Optional[str] = None
    ) -> None:
        assert mode in ("r", "r+", "w", "w+")
        self.mode = mode
        self.updates: dict[str, dict] = {}
        self._swapped_on_version_change: list[tuple[Module, Module]] = []
        self._swapped_on_mp_change: list[tuple[Module, Module]] = []
        self._unloaded_on_mp_change: list[Module] = []
        self._swapped_on_family_update: list[tuple[Module, Module]] = []
        self._swapped_explicitly: list[tuple[Module, Module]] = []
        self.opened = False
        self.stack: Optional[list] = None
        set_global_tcl()
        self.shell = shell_factory(shell)
        self.initialize(modulepath=modulepath)

    def restore(self):
        initial_data = self.initialization_data()
        if initial_data is None:
            xio.warn("No initialization data found")
            return
        for key, val in initial_data.items():
            os.environ[key] = val
        self.updates = {}
        self._swapped_on_version_change = []
        self._swapped_on_mp_change = []
        self._unloaded_on_mp_change = []
        self._swapped_on_family_update = []
        self._swapped_explicitly = []
        self.opened = False
        self.stack = None
        self.initialize(modulepath=os.getenv("MODULEPATH"))

    def initialize(self, modulepath: Optional[str] = None):
        strings = {}
        for var in os.environ:
            if var.startswith(self.meta_fmt[:16]):
                strings[int(var[16:-2])] = os.environ[var]
        serialized = [strings[k] for k in sorted(strings)]
        self.modulepath: Modulepath
        data: dict[str, Any] = {}
        if not serialized:
            self.modulepath = Modulepath(modulepath)
        else:
            data.update(deserialize(serialized))
            path = os.pathsep.join(data["__modulepath__"])
            self.modulepath = Modulepath(path)
        loaded_modules = data.pop("__loaded__", [])
        for packed in loaded_modules:
            module = self.unpack_module(packed)
            module.is_loaded = True
            data.setdefault("__loaded__", []).append(module)
        if "__families__" in data:
            for family, module in data["__families__"].items():
                data["__families__"][family] = self.unpack_module(module)  # type: ignore
        self.data = data

    def __enter__(self):
        self.stack = []
        self.opened = True
        return self

    def __exit__(self, ex_type, ex_value, ex_traceback):
        if ex_type is None and self.mode.startswith(("a", "w")):
            self.dump()
        self.opened = False
        self.stack = None

    @contextmanager
    def disable_trace(self):
        self.save_mode = self.mode
        self.mode = self.mode.rstrip("+")
        yield
        self.mode = self.save_mode

    def uinit(self):
        for var in os.environ:
            if var.startswith(self.ini_fmt[:15]):
                return
        chunk_size = config.get("serialize_chunk_size")
        chunks = serialize(dict(os.environ), chunk_size=chunk_size)
        for i, chunk in enumerate(chunks, start=1):
            self.setenv(self.ini_fmt % i, chunk)
        try:
            self.restore_module_collection("default")
        except CollectionNotFoundError:
            pass

    def refresh_module_cache(self) -> None:
        self.modulepath.refresh_cache()

    def clear_module_cache(self) -> None:
        self.modulepath.clear_cache()

    def show_module_cache(self) -> None:
        self.modulepath.show_cache()

    def initialization_data(self):
        strings = {}
        for var in os.environ:
            if var.startswith(self.ini_fmt[:15]):
                strings[int(var[15:-2])] = os.environ[var]
        serialized = [strings[k] for k in sorted(strings)]
        if not serialized:
            return None
        return deserialize(serialized)

    def trace(self, string):
        if self.mode.endswith("+"):
            xio.print(string)

    @staticmethod
    def platform_dependent_variable_name(name: str) -> str:
        if sys.platform == "darwin" and name == "LD_LIBRARY_PATH":
            return "DYLD_LIBRARY_PATH"
        return name

    def unpack_module(self, kwds: dict) -> Module:
        module = module_from_dict(kwds)
        self.modulepath.update_module(module)
        return module

    def swapped_explicitly(self, old: Module, new: Module):
        self._swapped_explicitly.append((old, new))

    def swapped_on_version_change(self, old: Module, new: Module):
        self._swapped_on_version_change.append((old, new))

    def swapped_on_mp_change(self, old: Module, new: Module):
        self._swapped_on_mp_change.append((old, new))

    def unloaded_on_mp_change(self, old: Module):
        self._unloaded_on_mp_change.append(old)

    def swapped_on_family_update(self, old: Module, new: Module):
        self._swapped_on_family_update.append((old, new))

    def change_directory(self, path):
        dryrun = config.get("dryrun")
        xio.print(f"cd {path}", file=sys.stderr if dryrun else sys.stdout)

    def dump(self):
        assert self.mode.startswith("w")
        xio.trace("Dumping the environment")
        if not self.data and not self.updates:
            return
        file = StringIO()
        loaded = ":".join(_.fullname for _ in self.loaded_modules)
        self.setenv("LOADEDMODULES", loaded)
        lm_files = ":".join(_.file for _ in self.loaded_modules)
        self.setenv("_LMFILES_", lm_files)
        self.setenv(Modulepath.varname, str(self.modulepath))
        self.data["__modulepath__"] = self.modulepath.roots
        data = self.json_serializable()
        chunk_size = config.get("serialize_chunk_size")
        chunks = serialize(data, chunk_size=chunk_size)
        for i, chunk in enumerate(chunks, start=1):
            self.setenv(self.meta_fmt % i, chunk)
        self.shell.format_env_mods(env_mods(self.updates), file=file)
        text = file.getvalue()
        dryrun = config.get("dryrun")
        if self.mode == "w+":  # pragma: no cover
            xio.print("\nDo you want to apply the environment modifications? (y/n) ", end="")
            response = input()
            if response.lower() not in ("y", "yes"):
                xio.print("Environment modifications will not be applied")
                return
        xio.print(text.rstrip(), file=sys.stderr if dryrun else sys.stdout)
        if self._swapped_explicitly:
            xio.print(self.format_swapped_explicitly().rstrip())
        if self._swapped_on_family_update:
            xio.print(self.format_swapped_on_family_update().rstrip())
        if self._unloaded_on_mp_change:
            xio.print(self.format_unloaded_on_mp_change().rstrip())
        if self._swapped_on_version_change:
            xio.print(self.format_swapped_on_version_change().rstrip())
        if self._swapped_on_mp_change:
            xio.print(self.format_swapped_on_mp_change().rstrip())

    def json_serializable(self):
        data = dict(self.data)
        loaded_modules = [m.asdict() for m in self.loaded_modules]
        data["__loaded__"] = loaded_modules
        for family, module in data.get("__families__", {}).items():
            data["__families__"][family] = module.asdict()
        return data

    def std_callbacks(self, mode) -> dict[str, Any]:
        callbacks = {
            "getenv": self.cb_getenv,
            "mkdirp": filesystem.mkdirp,
            "add_property": self.notimplemented("add_property"),
            "remove_property": self.notimplemented("remove_property"),
            "is_loaded": self.is_loaded,
        }
        assert mode in ("display", "load", "unload")
        if mode == "display":
            callbacks["mode"] = lambda: "load"
            callbacks["append_path"] = self.cb_noop
            callbacks["conflict"] = self.cb_noop
            callbacks["execute"] = self.cb_noop
            callbacks["family"] = self.cb_noop
            callbacks["load"] = self.cb_noop
            callbacks["prepend_path"] = self.cb_noop
            callbacks["prereq"] = self.cb_noop
            callbacks["prereq_any"] = self.cb_noop
            callbacks["pushenv"] = self.cb_noop
            callbacks["raw"] = self.cb_noop
            callbacks["remove_path"] = self.cb_noop
            callbacks["set_alias"] = self.cb_noop
            callbacks["setenv"] = self.cb_noop
            callbacks["set_shell_function"] = self.cb_noop
            callbacks["source"] = self.cb_noop
            callbacks["swap"] = self.cb_noop
            callbacks["try_load"] = self.cb_noop
            callbacks["unload"] = self.cb_noop
            callbacks["unsetenv"] = self.cb_noop
            callbacks["unset_alias"] = self.cb_noop
            callbacks["unset_shell_function"] = self.cb_noop
            callbacks["unuse"] = self.cb_noop
            callbacks["use"] = self.cb_noop
        elif mode == "unload":
            callbacks["append_path"] = self.unappend_path
            callbacks["conflict"] = self.cb_noop
            callbacks["execute"] = self.cb_noop
            callbacks["family"] = self.unset_family
            callbacks["load"] = self.cb_unload_module
            callbacks["mode"] = lambda: "unload"
            callbacks["prepend_path"] = self.unprepend_path
            callbacks["prereq"] = self.cb_noop
            callbacks["prereq_any"] = self.cb_noop
            callbacks["pushenv"] = self.unsetenv
            callbacks["raw"] = self.cb_noop
            callbacks["remove_path"] = self.unremove_path
            callbacks["set_alias"] = self.unset_alias
            callbacks["setenv"] = self.unsetenv
            callbacks["set_shell_function"] = self.unset_shell_function
            callbacks["source"] = self.unsource_file
            callbacks["swap"] = self.cb_unswap_modules
            callbacks["try_load"] = self.unload_module
            callbacks["unload"] = self.cb_noop
            callbacks["unsetenv"] = self.cb_noop
            callbacks["unset_alias"] = self.cb_noop
            callbacks["unset_shell_function"] = self.cb_noop
            callbacks["unuse"] = self.cb_noop
            callbacks["use"] = self.unuse
        else:
            callbacks["append_path"] = self.append_path
            callbacks["conflict"] = self.conflict
            callbacks["execute"] = self.execute
            callbacks["family"] = self.set_family
            callbacks["load"] = self.cb_load_module
            callbacks["mode"] = lambda: "load"
            callbacks["prepend_path"] = self.prepend_path
            callbacks["prereq"] = self.prereq_any
            callbacks["prereq_any"] = self.prereq_any
            callbacks["pushenv"] = self.pushenv
            callbacks["raw"] = self.raw_shell_command
            callbacks["remove_path"] = self.remove_path
            callbacks["set_alias"] = self.set_alias
            callbacks["setenv"] = self.cb_setenv
            callbacks["set_shell_function"] = self.set_shell_function
            callbacks["source"] = self.source_file
            callbacks["swap"] = self.cb_swap_modules
            callbacks["try_load"] = self.try_load_module
            callbacks["unload"] = self.cb_unload_module
            callbacks["unsetenv"] = self.unsetenv
            callbacks["unset_alias"] = self.unset_alias
            callbacks["unset_shell_function"] = self.unset_shell_function
            callbacks["unuse"] = self.unuse
            callbacks["use"] = self.cb_use
        return callbacks

    def find_module(self, name: str) -> Module:
        module = self.modulepath.find(name)
        if module is None:
            raise ModuleNotFoundError(name, self.modulepath)
        module.acquired_as = name
        return module

    def find_loaded_module(self, name: str) -> Optional[Module]:
        for module in self.loaded_modules:
            if module.matches(name):
                return module
        return None

    def purge_loaded_modules(self):
        for module in self.loaded_modules[::-1]:
            if module.is_loaded:
                self._unload_module(module)

    def reset_module_cache(self):
        self.modulepath.reset_cache()

    @property
    def collections_file(self):
        return os.path.join(config.cache_dir(), "collections.json")

    def dump_collections(self, data):
        if "collections" not in data:
            data = {"collections": data}
        filesystem.mkdirp(os.path.dirname(self.collections_file))
        with open(self.collections_file, "w") as fh:
            json.dump(data, fh, indent=2)

    def load_collections(self) -> dict:
        f = self.collections_file
        if os.path.exists(f):
            collections = json.load(open(f))["collections"]
        else:
            collections = {}
        return collections

    def save_module_collection(self, name: Optional[str] = None):
        """Save the loaded moduls to a collection"""
        name = name or "default"
        collection = []
        for module in self.loaded_modules:
            md = module.asdict()
            md["refcount"] = 0
            collection.append(md)
        collections = self.load_collections()
        collections[name] = collection
        self.dump_collections(collections)
        xio.info(f"Module collection {name} saved")

    def get_module_collection(self, name: Optional[str] = None) -> Optional[Any]:
        name = name or "default"
        collections = self.load_collections()
        return collections.get(name)

    def remove_module_collection(self, name: str):
        collections = self.load_collections()
        collections.pop(name, None)
        self.dump_collections(collections)
        xio.info(f"Module collection {name} removed")

    def restore_module_collection(self, name: str):
        collection = self.get_module_collection(name)
        if collection is None:
            raise CollectionNotFoundError(f"{name}: collection not found")
        self.purge_loaded_modules()
        roots = list(unique([x["root"] for x in collection]))
        for root in roots[::-1]:
            self.modulepath.prepend_path(root)
        for info in collection:
            module = self.unpack_module(info)
            self._load_module(module)

    def format_module_collection(self, name: str, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        collection = self.get_module_collection(name)
        if collection is None:
            raise CollectionNotFoundError(f"{name}: collection not found")
        file.write(f"{name}:\n")
        for info in collection:
            module = self.unpack_module(info)
            if module.uopts:
                file.write(f"  load({module.fullname}, opts={module.uopts!r})\n")
            else:
                file.write(f"  load({module.fullname})\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_available_collections(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        collections = self.load_collections()
        file.write("Available module collections:\n")
        elements = sorted(collections.keys())
        output = xio.colify(elements, indent=4)
        file.write(output)
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def reload_module(self, name: str) -> Module:
        module = self.find_module(name)
        if module.is_loaded:
            self._unload_module(module)
        return self._load_module(module)

    def load_module(
        self, name: str, opts: Optional[list[Union[str, tuple[str, str]]]] = None
    ) -> Module:
        module = self.find_module(name)
        if module.is_loaded:
            xio.warn(f"{module.fullname}: module loaded. Use 'module reload'")
            return module
        module.set_opts(opts)
        return self._load_module(module)

    def try_load_module(self, name: str) -> Optional[Module]:
        try:
            return self.cb_load_module(name)
        except Exception:
            return None

    def load_first_module(self, *names: str) -> Module:
        for name in names:
            try:
                module = self.find_module(name)
            except ModuleNotFoundError:
                continue
            break
        else:
            raise ModuleNotFoundError(names[0], self.modulepath)
        if module.is_loaded:
            xio.warn(f"{module.fullname}: module loaded. Use 'module reload'")
            return module
        return self._load_module(module)

    def _load_module(self, module: Module, opts: Optional[list[str]] = None) -> Module:
        assert self.stack is not None
        self.stack.append((module, "load"))
        # See if a module of the same name is already loaded. If so, swap that
        # module with the requested module
        for other in self.loaded_modules:
            if other.name == module.name:
                self._swap_modules(other, module)
                self.swapped_on_version_change(other, module)
                return module

        with filesystem.working_dir(module.dirname):
            try:
                checkpoint = util.checkpoint.create(self)
                module.exec("load", self)
            except ContinueError:
                pass
            except BreakError:
                util.checkpoint.restore(self, checkpoint)
                return module
            except FamilyLoadedError as e:
                assert e.module.is_loaded, f"_load_module: {e.module} is not loaded"
                assert not module.is_loaded, f"_load_module: {module} is not loaded"
                self.swapped_on_family_update(e.module, module)
                return self._swap_modules(e.module, module)
            finally:
                util.checkpoint.delete(self, checkpoint)
                self.stack.pop()
            self.loaded_modules.append(module)
            module.is_loaded = True
            module.refcount += 1
        return module

    def execute(self, command: str) -> int:
        args = shlex.split(command)
        p = subprocess.Popen(args)
        p.wait()
        return p.returncode

    def prereq_any(self, *names: str) -> None:
        for name in names:
            m = self.find_loaded_module(name)
            if m is not None:
                return
        raise PrerequisiteMissingError

    def is_loaded(self, name: str) -> bool:
        for module in self.loaded_modules:
            if module.matches(name):
                return True
        return False

    def show_module(self, name: str, opts: Optional[list[Union[str, tuple[str, str]]]] = None):
        if self.mode != "r+":
            raise ValueError("show_module requires environ.mode='r+'")
        with self.disable_trace():
            self.mode == "w"
            module = self.find_module(name)
            if module.is_loaded:
                self._unload_module(module)
            module.set_opts(opts)
        self.mode == "r+"
        self._load_module(module)

    def _loadh(self, name: str) -> Module:
        assert self.mode.startswith("r"), "_loadh: incorrect mode"
        module = self.find_module(name)
        module.exec("display", self)
        return module

    def format_module_help(self, name: str, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        module = self._loadh(name)
        module.format_help(file=file)
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_module_whatis(self, name: str, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        module = self._loadh(name)
        module.format_whatis(file=file)
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_module_info(self, name: str, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        found = False

        def getter(m):
            if m.fullname == name:
                return True
            elif m.name == name:
                return True
            elif m.matches(name):
                return True

        for root in self.modulepath.roots:
            module = self.modulepath.find_at(root, getter)
            if module is None:
                continue
            found = True
            module.acquired_as = name
            module.format_info(file=file)
        if not found:
            raise ModuleNotFoundError(name, self.modulepath)
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def unload_module(self, name: str) -> Module:
        module = self.find_loaded_module(name)
        if module is None:
            raise ValueError(f"{name} not found in loaded modules")
        return self._unload_module(module)

    def _unload_module(self, module: Module) -> Module:
        assert self.stack is not None
        self.stack.append((module, "unload"))
        module.exec("unload", self)
        module.refcount -= 1
        if module.refcount == 0:
            self.loaded_modules.pop(self.loaded_modules.index(module))
            module.is_loaded = False
        self.stack.pop()
        return module

    def swap_modules(self, name1: str, name2: str) -> Optional[Module]:
        module1 = self.find_loaded_module(name1)
        if module1 is None:
            return self.load_module(name2)
        module2 = self.find_loaded_module(name2)
        if module2 is not None:
            xio.warn(f"{module2.fullname} is already loaded!")
            return module2
        module1 = self.find_module(name1)
        module2 = self.find_module(name2)
        self._swap_modules(module1, module2)
        self.swapped_explicitly(module1, module2)
        return module2

    def _swap_modules(self, module1, module2, maintain_state=False):
        """The general strategy of swapping is to unload all modules in reverse
        order back to the module to be swapped.  That module is then unloaded
        and its replacement loaded.  Afterward, modules that were previously
        unloaded are reloaded.

        On input:
            module1 is loaded
            module2 is not loaded

        On output:
            module1 is not loaded
            module2 is loaded

        The condition that module2 is not loaded on input is not strictly true
        In the case that a module is reloaded, module1 and module2 would be
        the same, so module2 would also be loaded.

        """
        assert module1.is_loaded, f"_swap_modules: {module1} is not loaded"

        # Before swapping, unload modules and later reload
        loaded_modules = self.loaded_modules
        opts = dict([(m.name, m.uopts) for m in loaded_modules])
        for i, other in enumerate(loaded_modules):
            if other.name == module1.name:
                # All modules coming after this one will be unloaded and
                # reloaded
                to_unload_and_reload = loaded_modules[i:]
                break
        else:  # pragma: no cover
            raise NoModulesToSwapError

        # Unload any that need to be unloaded first
        for other in to_unload_and_reload[::-1]:
            self._unload_module(other)
        assert other.name == module1.name, f"_swap_modules: {other.name} != {module1.name}"

        # Now load it
        self._load_module(module2)

        # Reload any that need to be unloaded first
        for other in to_unload_and_reload[1:]:
            if maintain_state:
                this_module = self.modulepath.find_at(other.root, lambda m: m.path == other.path)
            else:
                this_module = self.find_module(other.acquired_as)
            if this_module is None:
                # The only way this_module is None is if a swap of modules
                # caused a change to MODULEPATH making this module
                # unavailable.
                self.unloaded_on_mp_change(other)
                continue

            if this_module.file != other.file:
                self.swapped_on_mp_change(other, this_module)

            # Now load the thing
            if opts.get(this_module.name):
                this_module.set_opts(opts[this_module.name])
            self._load_module(this_module)

        return module2

    @property
    def loaded_modules(self) -> list[Any]:
        return self.data.setdefault("__loaded__", [])

    @property
    def families(self):
        return self.data.setdefault("__families__", {})

    @property
    def sourced_files(self):
        return self.data.setdefault("__sourced_files__", {})

    def meta_data(self, name: str) -> dict:
        all_meta_data = self.data.setdefault("__meta__", {})
        return all_meta_data.setdefault(name, {})

    def remove_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.unuse(path)
        xio.trace(f"removing {path!r} from ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": 0, "position": -1, "priority": -1})
        if path in current:
            d["position"] = current.index(path)
            current.pop(d["position"])
            d["count"] = current.count(path)
        if current:
            self.setenv(name, util.join(current, sep=sep))
        else:
            meta.pop(path, None)
            self.unsetenv(name)

    def unremove_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.use(path)
        xio.trace(f"unremoving {path!r} from ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": 0, "position": -1, "priority": -1})
        index = d.pop("position", -1)
        d["count"] -= 1
        if index == -1 or index >= len(current):
            current.append(path)
        else:
            current.insert(index, path)
        if d["count"] <= 0:
            meta.pop(path, None)
        if current:
            self.setenv(name, util.join(current, sep=sep))
        else:
            meta.pop(path, None)
            self.unsetenv(name)

    def unprepend_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.unuse(path)
        xio.trace(f"unprepending {path!r} from ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": 0, "priority": -1})
        if d["count"] == 0 and path in current:
            xio.warn("Inconsistent refcount state")
            d["count"] = current.count(path)
            if config.get("debug"):
                raise ValueError("Inconsistent refcount state")
        if path in current:
            current.pop(current.index(path))
            d["count"] -= 1
        if d["count"] == 0:
            meta.pop(path, None)
        if current:
            self.setenv(name, util.join(current, sep=sep))
        else:
            meta.pop(path, None)
            self.unsetenv(name)

    def unappend_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.unuse(path)
        xio.trace(f"unapppending {path!r} from ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": 0, "priority": -1})
        if d["count"] == 0 and path in current:
            xio.warn("Inconsistent refcount state")
            d["count"] = current.count(path)
            if config.get("debug"):
                raise ValueError("Inconsistent refcount state")
        if path in current:
            current.pop(current.index(path))
            d["count"] -= 1
        if d["count"] == 0:
            meta.pop(path, None)
        if current:
            self.setenv(name, util.join(current, sep=sep))
        else:
            meta.pop(name, None)
            self.unsetenv(name)

    # -------------------------------------------------------------------------------- #
    # --- Implementation of callback methods ----------------------------------------- #
    # -------------------------------------------------------------------------------- #
    def cb_getenv(self, name):
        return os.getenv(name)

    def cb_setenv(self, name: str, value: str, traceit: bool = True) -> None:
        return self.setenv(name, value, traceit=traceit)

    def cb_load_module(self, name: str):
        module = self.find_module(name)
        if module.is_loaded:
            module.refcount += 1
        else:
            self._load_module(module)

    def cb_use(self, path, append=False):
        assert self.stack is not None
        module, _ = self.stack[-1]
        module.unlocks_path(path)
        self.use(path, append=append)

    def cb_unload_module(self, name: str) -> None:
        try:
            module = self.find_module(name)
        except Exception:
            xio.warn(f"{name} not found")
            return
        if not module.is_loaded:
            xio.warn(f"{name} not loaded")
            return
        if module.refcount > 1:
            module.refcount -= 1
        else:
            self._unload_module(module)

    def cb_swap_modules(self, name1: str, name2: str) -> None:
        module2 = self.find_loaded_module(name2)
        if module2 is not None:
            module2.refcount += 1
            return
        module2 = self.find_module(name2)
        module1 = self.find_loaded_module(name1)
        if module1 is None:
            self._load_module(module2)
            return
        module1 = self.find_module(name1)
        self._swap_modules(module1, module2)

    def cb_unswap_modules(self, name1: str, name2: str) -> None:
        return self.cb_swap_modules(name2, name1)

    def cb_noop(self, *args: Any, **kwargs: Any) -> None:
        pass

    def setenv(self, name: str, value: str, traceit: bool = True) -> None:
        xio.trace(f"setting ${name} to {value!r}")
        os.environ[name] = value
        env = self.updates.setdefault("__env__", {})
        env[name] = value
        tcl("set", f"env({name})", value)
        if traceit and not name.startswith(self.internal_vars):
            self.trace(f"setenv({name!r}, {value!r})")

    def pushenv(self, name: str, value: str, traceit: bool = True) -> None:
        xio.trace(f"setting ${name} to {value!r}")
        if name in os.environ:
            self.data.setdefault("__pushed__", {})[name] = value
        os.environ[name] = value
        self.updates.setdefault("__env__", {})[name] = value
        tcl("set", f"env({name})", value)
        if traceit and not name.startswith(self.internal_vars):
            self.trace(f"setenv({name!r}, {value!r})")

    def unsetenv(self, name: str, _: Optional[str] = None, traceit: bool = True) -> None:
        xio.trace(f"unsetting ${name}")
        os.environ.pop(name, None)
        self.updates.setdefault("__env__", {})[name] = None
        tcl("catch", f"unset env({name})", "")
        if name in self.data.get("__pushed__", {}):
            value = self.data["__pushed__"].pop(name)
            os.environ[name] = value
            tcl("set", f"env({name})", value)
        if traceit and not name.startswith(self.internal_vars):
            self.trace(f"unset({name!r})")

    def set_alias(self, name: str, body: str) -> None:
        xio.trace(f"aliasing ${name} to {body!r}")
        aliases = self.updates.setdefault("__aliases__", {})
        aliases[name] = body
        self.trace(f"set_alias({name!r}, {body!r})")

    def unset_alias(self, name: str, _: Optional[str] = None) -> None:
        xio.trace(f"unaliasing ${name}")
        aliases = self.updates.setdefault("__aliases__", {})
        aliases[name] = None
        self.trace(f"unset_alias({name!r})")

    def set_shell_function(self, name: str, body: str) -> None:
        xio.trace(f"setting shell function ${name} to {body!r}")
        shellfuns = self.updates.setdefault("__shell_functions__", {})
        shellfuns[name] = body
        self.trace(f"set_shell_function({name!r}, {body!r})")

    def unset_shell_function(self, name: str, _: Optional[str] = None) -> None:
        xio.trace(f"unsetting shell function ${name}")
        shellfuns = self.updates.setdefault("__shell_functions__", {})
        shellfuns[name] = None
        self.trace(f"unset_shell_function({name!r}")

    def raw_shell_command(self, command: str) -> None:
        shellcmds = self.updates.setdefault("__shell_commands__", {})
        shellcmds[len(shellcmds)] = command
        self.trace(command)

    def append_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.use(path, append=True)
        xio.trace(f"apppending {path!r} to ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": -1, "priority": -1})
        if d["count"] == -1:
            d["count"] = current.count(path)
        current.append(path)
        d["count"] += 1
        self.setenv(name, util.join(current, sep=sep), traceit=False)
        self.trace(f"append_path({name!r}, {path!r}, sep={sep!r})")

    def prepend_path(self, name: str, path: str, sep: str = os.pathsep) -> None:
        name = self.platform_dependent_variable_name(name)
        if name == Modulepath.varname:
            return self.use(path)
        xio.trace(f"prepending {path!r} to ${name}")
        current = util.split(os.getenv(name), sep=sep)
        meta = self.meta_data(name)
        d = meta.setdefault(path, {"count": -1, "priority": -1})
        if d["count"] == -1:
            d["count"] = current.count(path)
        current.insert(0, path)
        d["count"] += 1
        self.setenv(name, util.join(current, sep=sep), traceit=False)
        self.trace(f"prepend_path({name!r}, {path!r}, sep={sep!r})")

    def set_family(self, family: str) -> None:
        assert self.stack is not None
        module, _ = self.stack[-1]
        current = self.families.get(family)
        if current:
            # Attempting to load module of same family
            root, path = current.root, current.path
            other = self.modulepath.find_at(root, lambda m: m.path == path)
            raise FamilyLoadedError(other)
        self.trace(f"family({family})")
        module.family = family
        self.families[family] = module

    def unlocked_by(self, module):
        """Returns the module[s] that unlock this `module`, if any"""
        unlocks_me = []
        dirname = module.root
        for other in self.loaded_modules[::-1]:
            if other.unlocks(path=dirname):
                unlocks_me.append(other)
                dirname = other.root
        module.unlocked_by = list(unlocks_me[::-1])
        return module.unlocked_by

    def use(self, path, append=False):
        """Add path to MODULEPATH"""
        path = expand_name(path)
        if append:
            return self.modulepath.append_path(path)
        else:
            prepended = self.modulepath.prepend_path(path)
            if prepended is None:
                xio.warn(f"No modules were found in {path}")
                return
            bumped = self.determine_swaps_due_to_prepend(prepended)
            for old, new in bumped:
                assert old.is_loaded, f"use: {old} is not loaded"
                if new.fullname == old.acquired_as:
                    new.acquired_as = old.acquired_as
                else:
                    new.acquired_as = new.fullname
                self._swap_modules(old, new)
                self.swapped_on_mp_change(old, new)
        return prepended

    def unuse(self, path, **kwargs):
        """Remove path to MODULEPATH"""
        path = expand_name(path)
        if path not in self.modulepath:
            return
        removed = self.modulepath.remove(path)
        orphaned = self.determine_swaps_due_to_unuse(removed)
        for orphan in orphaned[::-1]:
            self._unload_module(orphan[0])
        # Load modules bumped by removal of dirname from MODULEPATH
        for orphan in orphaned:
            if orphan[1] is None:
                # No longer available!
                self.unloaded_on_mp_change(orphan[0])
            else:
                self._load_module(orphan[1])
                self.swapped_on_mp_change(orphan[0], orphan[1])
        return removed

    def determine_swaps_due_to_unuse(self, popped_modules):
        """Determine with of the popped modules should be swapped for modules that
        became available after removing a directory from the modulepath

        Parameters
        ----------
        popped_modules : list of Module
            Modules no longer available due to their modulepath being removed

        Return
        ------
        orphans : list of tuple
            orphans[i][0] loaded module left orphaned
            orphans[i][1] module to be loaded in its place, or None

        """

        # Determine which modules may have moved up in priority due to removal
        # of directory from path. If they have the same name as an orphan, it
        # will be loaded in the orphans place
        orphaned = [m for m in popped_modules if m.is_loaded]
        for i, orphan in enumerate(orphaned):
            other = self.modulepath.find_at(orphan.root, lambda m: m.path == orphan.path)
            if other is not None:
                orphaned[i] = (orphan, other)
                break
            else:
                orphaned[i] = (orphan, None)
        return orphaned

    def unset_family(self, family: str) -> None:
        families = self.families
        families.pop(family, None)

    def notimplemented(self, name):
        return lambda *a, **kw: xio.warn(f"callback {name} is not implemented")

    def conflict(self, *conflicting):
        """The module we are trying to load, `module` conflicts with the module
        given by `conflicting_module`"""
        assert self.stack is not None
        current, _ = self.stack[-1]
        lm_names = list(set([x for m in self.loaded_modules for x in [m.name, m.fullname]]))
        for other in conflicting:
            if other in lm_names:
                s = f"{other!r} conflicts with {current.fullname!r}"
                raise ConflictError(s)

    def determine_swaps_due_to_prepend(self, prepended_modules):
        """Determine with modules lost precedence and need to be replaced

        Parameters
        ----------
        prepended_modules : list of Module
            These are modules that are now available from prepending their
            modulepath to modulecmd.modulepath

        Returns
        -------
        bumped : list of Module
            List of loaded modules that have lower precedence than a module of the
            same name in prepended_modules. These should be swapped

        """
        # Determine which modules changed in priority due to insertion of new
        # directory in to path
        bumped = []
        loaded_modules = list(self.loaded_modules)

        # Check for fullname first
        fullnames = [m.fullname for m in prepended_modules]
        for i, loaded_module in enumerate(loaded_modules):
            if loaded_module.fullname not in fullnames:
                continue
            ix = fullnames.index(loaded_module.fullname)
            prepended_module = prepended_modules[ix]
            if prepended_module.file != loaded_module.file:
                # The new module has the same name, but different filename. Since
                # new module has higher precedence (since its path was prepended to
                # modulepath), we swap them
                bumped.append((loaded_module, prepended_module))
                loaded_modules[i] = None
        names = [m.name for m in prepended_modules]
        for i, loaded_module in enumerate(loaded_modules):
            if loaded_module is None or loaded_module.name not in names:
                continue
            if loaded_module.acquired_as == loaded_module.fullname:  # pragma: no cover
                continue
            prepended_module = prepended_modules[names.index(loaded_module.name)]
            if prepended_module.file != loaded_module.file:
                bumped.append((loaded_module, prepended_modules[i]))
        return bumped

    def format_loaded(self, file=None):
        fown = file is None
        file = file or StringIO()
        loaded = self.loaded_modules
        if not loaded:
            file.write("No loaded modules\n")
        else:
            elements = []
            for i, module in enumerate(loaded):
                elements.append(f"{i+1}) {module.display_name()}")
            file.write("Currently loaded modules\n")
            output = xio.colify(elements, indent=4)
            file.write(output + "\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()

    def format_swapped_explicitly(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        file.write(xio.colorize("The following modules have been %(g)sswapped%(e)s\n"))
        for i, (m1, m2) in enumerate(self._swapped_explicitly):
            file.write(f"  {i + 1}) {m1.fullname} => {m2.fullname}\n")
        file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_swapped_on_mp_change(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        s = xio.colorize(
            "The following modules have been %(g)supdated on a MODULEPATH change%(e)s\n"
        )
        file.write(s)
        for i, (m1, m2) in enumerate(self._swapped_on_mp_change):
            file.write(f"  {i + 1}) {m1.fullname} => {m2.fullname}\n")
        file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_swapped_on_version_change(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        s = xio.colorize("The following modules have been %(g)supdated on a version change%(e)s\n")
        file.write(s)
        for i, (m1, m2) in enumerate(self._swapped_on_mp_change):
            file.write(f"  {i + 1}) {m1.fullname} => {m2.fullname}\n")
        file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_unloaded_on_mp_change(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        lm_files = [_.file for _ in self.loaded_modules]
        unloaded = [_ for _ in self._unloaded_on_mp_change if _.file not in lm_files]
        s = xio.colorize(
            "The following modules have been %(G)sunloaded on a MODULEPATH change%(e)s\n"  # noqa: E501
        )
        file.write(s)
        for i, module in enumerate(unloaded):
            file.write(f"  {i + 1}) {module.fullname}\n")
        file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_swapped_on_family_update(self, file: Optional[TextIO] = None) -> Optional[str]:
        fown = file is None
        file = file or StringIO()
        s = xio.colorize(
            "The following modules in the same family have been "
            "%(G)supdated on a version change%(e)s\n"
        )
        file.write(s)
        for i, (m1, m2) in enumerate(self._swapped_on_family_update):
            file.write(f"  {i + 1}) {m1.fullname} => {m2.fullname} ({m1.family})\n")
        file.write("\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def format_avail(
        self,
        terse: bool = False,
        file: Optional[TextIO] = None,
        regex: Optional[str] = None,
        path: Optional[str] = None,
    ) -> Optional[str]:
        xio.trace("Formatting available modules")
        fown = file is None
        file = file or StringIO()
        regexp: Optional[Pattern] = None if regex is None else re.compile(regex)
        for root, modules in self.modulepath.traverse():
            if path and root != path:
                continue
            if terse:
                names = [m.fullname for m in modules]
                file.write(f"{root}:\n")
                for name in names:
                    if regexp is not None and not regexp.search(name):
                        continue
                    file.write(f"{name}\n")
            else:
                width = xio.terminal_size().columns
                names = []
                for module in modules:
                    name = module.fullname
                    if regexp is not None and not regexp.search(name):
                        continue
                    opts = []
                    if module.is_default:
                        if module.is_global_default:
                            color = "R"
                        elif module.is_explicit_default:
                            color = "M"
                        else:
                            color = "B"
                        opts.append(xio.colorize(f"%({color})sD%(e)s"))
                    if module.is_loaded:
                        opts.append(xio.colorize("%(G)sL%(e)s"))
                    if opts:
                        name += f" ({','.join(opts)})"
                    names.append(name)
                if regexp and not names:
                    continue
                root = root.replace(os.path.expanduser("~"), "~")
                output = xio.colify(names)
                file.write(xio.colorize(f" %(G)s{root}%(e)s: ").center(width, "-") + "\n")
                if not output.split():
                    output = xio.colorize("%(r)s(None)%(e)s").center(width)
                file.write(f"{output}\n\n")
        if fown:
            assert isinstance(file, StringIO)
            return file.getvalue()
        return None

    def source_file(self, shellscript: str) -> None:
        """Determine changes to the process environment as a result of sourcing
        `shellscript` and apply the changes to the environment

        Parameters
        ----------
        shellscript : str
            The file to source

        """
        modifications = self.shell.getsourcediff(shellscript)
        self.apply_sourced_changes(modifications)
        self.sourced_files[shellscript] = modifications

    def unsource_file(self, shellscript: str) -> None:
        """Determine changes to the process environment as a result of sourcing
        `shellscript` and apply the changes to the environment

        Parameters
        ----------
        shellscript : str
            The file to source

        """
        modifications = self.sourced_files.pop(shellscript, None)
        if modifications:
            self.unapply_sourced_changes(modifications)

    def apply_sourced_changes(self, modifications: dict) -> None:
        """Convert the state returned by source_file to a equivalent module commands"""
        for what, name, value in modifications.get("env", []):
            if what == "set":
                self.setenv(name, value)
            elif what == "unset":
                self.unsetenv(name)
            elif what == "prepend-path":
                self.prepend_path(name, value)
            elif what == "append-path":
                self.append_path(name, value)
            else:
                xio.debug(f"unknown env directive {what!r}")
        for what, name, value in modifications.get("function", []):
            if what == "unset":
                self.unset_shell_function(name)
            elif what == "set":
                self.set_shell_function(name, value)
            else:
                xio.debug(f"unknown function directive {what!r}")
        for what, name, value in modifications.get("alias", []):
            if what == "unset":
                self.unset_alias(name)
            elif what == "set":
                self.set_alias(name, value)
            else:
                xio.debug(f"unknown alias directive {what!r}")

    def unapply_sourced_changes(self, modifications: dict) -> None:
        """Convert the state returned by source_file to a equivalent module commands"""
        for what, name, value in modifications.get("env", []):
            if what == "set":
                self.unsetenv(name)
            elif what == "prepend-path":
                self.unprepend_path(name, value)
            elif what == "append-path":
                self.unappend_path(name, value)
        for what, name, value in modifications.get("function", []):
            if what == "set":
                self.unset_shell_function(name)
        for what, name, value in modifications.get("alias", []):
            if what == "set":
                self.unset_alias(name)


class env_mods:
    def __init__(self, arg: dict):
        self.variables = arg.get("__env__", {})
        self.aliases = arg.get("__aliases__", {})
        self.shell_functions = arg.get("__shell_functions__", {})
        self.raw_shell_commands = list(arg.get("__shell_commands__", {}).values())


def set_global_tcl() -> None:
    if not config.get("use_tkinter"):
        return

    import tkinter

    global global_tcl
    global_tcl = tkinter.Tcl()


def tcl(arg1: str, arg2: str, arg3: str) -> None:
    if global_tcl is not None:
        global_tcl.call(arg1, arg2, arg3)


class CollectionNotFoundError(Exception):
    pass


class ModuleNotFoundError(Exception):
    def __init__(self, name: str, mp: Optional[Modulepath] = None):
        msg = f"{name!r} is not a module.  See 'module avail'"
        if mp:
            candidates = mp.candidates(name)
            if candidates:  # pragma: no cover
                msg += "\n\nDid you mean one of these?"
                msg += "\n\t{0}".format("\t".join(candidates))
        super(ModuleNotFoundError, self).__init__(msg)


class FamilyLoadedError(Exception):
    def __init__(self, other):
        self.module = other
        super(FamilyLoadedError, self).__init__(
            f"{other.family}: module of same family already loaded"
        )


class NoModulesToSwapError(Exception):
    pass


class ConflictError(Exception):
    pass


class PrerequisiteMissingError(Exception):
    pass
