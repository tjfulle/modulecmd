import argparse
import json
import os
import sys
from contextlib import redirect_stdout
from io import StringIO
from typing import Any
from typing import Optional

from . import config
from . import environ
from . import rst2ansi
from . import xio
from .module import isvariant
from .shell import factory as shell_factory
from .shell import shells

user_shell = None


class MainContext:
    def __init__(self, profile: bool) -> None:
        self.profiler = None if not profile else get_profiler()

    def __enter__(self):
        self.start()

    def __exit__(self, *args):
        self.stop()
        self.print()

    def start(self) -> None:
        if self.profiler is None:
            return None
        elif hasattr(self.profiler, "start"):
            self.profiler.start()
        else:
            self.profiler.enable()

    def stop(self) -> None:
        if self.profiler is None:
            return
        elif hasattr(self.profiler, "stop"):
            self.profiler.stop()
        else:
            self.profiler.disable()

    def print(self) -> None:
        if self.profiler is None:
            return
        with redirect_stdout(sys.stderr):
            if hasattr(self.profiler, "print"):
                self.profiler.print()
            else:
                import pstats

                p = pstats.Stats(self.profiler)
                sort_keys = (pstats.SortKey.CUMULATIVE, pstats.SortKey.TIME)
                p.strip_dirs().sort_stats(*sort_keys).print_stats(20)


def get_profiler() -> Any:
    try:
        import pyinstrument

        return pyinstrument.Profiler()
    except ImportError:
        import cProfile

        return cProfile.Profile()


def set_shell(arg: str) -> None:
    global user_shell
    user_shell = arg


def load(*args_in: str, confirm: bool = False) -> None:
    ns = argparse.Namespace(name=args_in[0], opts=[])
    groups = [ns]
    xio.trace(f"Loading the following modules: {', '.join(g.name for g in groups)}")
    for arg in args_in[1:]:
        if isvariant(arg):
            ns.opts.append(arg)
        else:
            ns = argparse.Namespace(name=arg, opts=[])
            groups.append(ns)
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        for group in groups:
            env.load_module(group.name, opts=group.opts)


def reload(name: str, confirm: bool = False) -> None:
    xio.trace(f"reloading {name}")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.reload_module(name)


def swap(name1: str, name2: str, confirm: bool = False) -> None:
    xio.trace(f"swapping {name1} and {name2}")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.swap_modules(name1, name2)


def unload(*names: str, confirm: bool = False) -> None:
    xio.trace(f"Unloading the following modules: {', '.join(names)}")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        for name in names:
            env.unload_module(name)


def purge(confirm: bool = False):
    xio.trace("Purging loaded modules")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.purge_loaded_modules()


def reset_module_cache():
    xio.trace("Resetting module cache")
    with environ.environ(mode="r") as env:
        env.reset_module_cache()


def show(*names: str) -> None:
    xio.trace(f"Showing sided effects of loading {', '.join(names)}")
    with environ.environ(mode="r+", shell=user_shell) as env:
        for name in names:
            env.show_module(name)


def show_meta_data() -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        idata = env.initialization_data()
        if idata:
            xio.print("Initial environment = " + json.dumps(idata, indent=4))
        xio.print("Environment data = " + json.dumps(env.json_serializable(), indent=4))


def show_modulepath(terse: bool = True) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        if terse:
            xio.print("\n".join(f"- {_}" for _ in env.modulepath.roots))
        else:
            for root, modules in env.modulepath.traverse():
                xio.print(f"{root}:")
                for module in modules:
                    xio.print(f"   {module}")


def show_moduleinfo(name: str) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        info = env.format_module_info(name)
        xio.print(info.strip())


def list_loaded() -> None:
    xio.trace("Listing loaded modules")
    with environ.environ(mode="r", shell=user_shell) as env:
        xio.print(env.format_loaded().rstrip())


def avail(terse: bool = False, regex: Optional[str] = None, path: Optional[str] = None) -> None:
    xio.trace("Showing available modules")
    with environ.environ(mode="r", shell=user_shell) as env:
        file = StringIO()
        env.format_avail(file=file, terse=terse, regex=regex, path=path)
    string = file.getvalue()
    xio.print(f"\n{string.rstrip()}")


def find(name: str) -> None:
    xio.trace(f"Finding {name!r}")
    with environ.environ(mode="r", shell=user_shell) as env:
        module = env.find_module(name)
        xio.print(module.file)


def cat(*names: str) -> None:
    xio.trace(f"Concatenating and printing {', '.join(names)}")
    with environ.environ(mode="r", shell=user_shell) as env:
        for name in names:
            module = env.find_module(name)
            xio.print(open(module.file).read())


def print_help(name: str) -> None:
    xio.trace(f"Printing help message for {name}")
    with environ.environ(mode="r", shell=user_shell) as env:
        file = StringIO()
        env.format_module_help(name, file=file)
        xio.print(file.getvalue().rstrip())


def print_whatis(name: str) -> None:
    xio.trace(f"Printing short description for {name}")
    with environ.environ(mode="r", shell=user_shell) as env:
        file = StringIO()
        env.format_module_whatis(name, file=file)
        xio.print(file.getvalue().rstrip())


def use(path: str, append: bool = False, confirm: bool = False) -> None:
    xio.trace(f"Adding {path} to MODULEPATH")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.use(path, append=append)


def unuse(path: str, confirm: bool = False) -> None:
    xio.trace(f"Removing {path} from MODULEPATH")
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.unuse(path)


def ignore_modules(path: str) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.modulepath.add_path_to_ignore(path)


def clear_cache() -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.clear_module_cache()


def show_cache() -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.show_module_cache()


def refresh_cache() -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.refresh_module_cache()


def visit_path(path) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        if path == "MODULECMD_PREFIX":
            path = os.path.dirname(os.path.dirname(__file__))
        env.change_directory(path)


def save_collection(name: str) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.save_module_collection(name)


def remove_collection(name: str) -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        env.remove_module_collection(name)


def restore_collection(name: str, confirm: bool = False) -> None:
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        env.restore_module_collection(name)


def show_collection(name: str, confirm: bool = False) -> None:
    mode = "w" if not confirm else "w+"
    with environ.environ(mode=mode, shell=user_shell) as env:
        xio.print(env.format_module_collection(name).rstrip())


def show_available_collections() -> None:
    with environ.environ(mode="r", shell=user_shell) as env:
        xio.print(env.format_available_collections().rstrip())


def init(shell_name: str) -> None:
    shell = shell_factory(shell_name)
    shell.init()


def __init__() -> None:
    with environ.environ(mode="w", shell=user_shell) as env:
        env.uinit()


def print_config(show_path: bool = False) -> None:
    if show_path:
        xio.print(f"cache directory: {config.cache_dir()}")
        xio.print(f"configuration directory: {config.config_dir()}")
        xio.print(f"configuration file: {config.config_file()}")
    else:
        xio.print(config.prettystr())


def set_config_var(name: str, value: Any) -> None:
    config.set(name, value, "user")


def pop_config_var(name: str) -> None:
    config.pop(name, "user")


def show_guide(guide):
    try:
        import docutils
    except ImportError:
        raise ValueError("Guides require docutils module") from None
    try:
        from docutils import core  # noqa: F401
        from docutils import nodes  # noqa: F401
    except ImportError:
        raise ValueError("Unable to import several docutils components") from None
    this_dir = os.path.dirname(__file__)
    filename = os.path.join(this_dir, f"../docs/{guide}.rst")
    with redirect_stdout(sys.stderr):
        xio.pager(rst2ansi.rst2ansi(open(filename, "r").read()))


class argument_parser(argparse.ArgumentParser):
    def print_usage(self, file=None):
        super(argument_parser, self).print_usage(file=sys.stderr)

    def print_help(self, file=None):
        super(argument_parser, self).print_help(file=sys.stderr)

    def add_argument(self, *args, **kwargs):
        default = kwargs.get("default")
        help_message = kwargs.get("help")
        if kwargs.get("action") == "store_true":
            kwargs["default"] = False
        if default not in (None, argparse.SUPPRESS) and help_message is not None:
            kwargs["help"] = f"{help_message} [default: {default}]"
        super(argument_parser, self).add_argument(*args, **kwargs)


def modulecmd_parser() -> argument_parser:
    parser = argument_parser(prog="module")
    parser.add_argument(
        "-l",
        "--log-level",
        type=int,
        choices=(0, 1, 2, 3, 4),
        help="Log level.  0: error, 1: warn, 2: info, 3: debug, 4: trace [default: 2]",
    )
    parser.add_argument(
        "--dryrun",
        action="store_true",
        default=False,
        help="Print what would be executed by shell, but don't execute",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="Run in debug mode",
    )
    parser.add_argument(
        "--profile",
        action="store_true",
        default=False,
        help="Run in profile mode",
    )

    parent = parser.add_subparsers(dest="subcommand", metavar="subcommand")
    parent.add_parser("__init__")

    p = parent.add_parser("init", help="Initialize modulecmd")
    p.add_argument("shell", choices=shells, help="Shell to initialize")

    pp = parent.add_parser("config", help="Manage modulecmd configuration")
    sp = pp.add_subparsers(dest="c_cmd", metavar="config subcommands")
    p = sp.add_parser("show", help="Show the modulecmd config")
    p.add_argument(
        "-p",
        action="store_true",
        default=False,
        help="Show path to configuration settings file",
    )
    p = sp.add_parser("set", help="Set a modulecmd config variable")
    p.add_argument("name", help="Configuration setting name")
    p.add_argument("value", help="Configuration setting value")
    p = sp.add_parser("pop", help="Remove a modulecmd config setting")
    p.add_argument("name", help="Configuration setting name")

    p = parent.add_parser("avail", help="Print available modules")
    p.add_argument("-p", metavar="PATH", help="Print modules in this path only")
    p.add_argument("-t", "--terse", action="store_true", help="Print terse (single column) output")
    p.add_argument(
        "regex",
        nargs=argparse.REMAINDER,
        help="Search for module names matching this regular expression",
    )

    p = parent.add_parser("find", help="Print the path module path")
    p.add_argument("name", help="Module name")

    p = parent.add_parser("load", help="Load the module[s]")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("names", nargs="+", help="Module names")

    p = parent.add_parser("swap", help="Swap modules")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("name1", help="Module to unload")
    p.add_argument("name2", help="Module to load")

    p = parent.add_parser("reload", help="Reload the module")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("name", help="Module name")

    p = parent.add_parser("unload", help="Unload the module[s]")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("names", nargs="+", help="Module names")

    p = parent.add_parser("cat", help="Concatenate and print module[s]")
    p.add_argument("names", nargs="+", help="Module names")

    p = parent.add_parser("show", help="Show the commands that would be issued by module load")
    p.add_argument("names", nargs="+", help="Module names")

    p = parent.add_parser("purge", help="Unload all loaded modules")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    parent.add_parser("list", aliases=["ls"], help="List loaded modules")

    p = parent.add_parser("help", help="Print a module's help")
    p.add_argument("name", help="Module name")

    p = parent.add_parser("whatis", help="Print a module's short description")
    p.add_argument("name", help="Module name")

    p = parent.add_parser("use", help="Add path to MODULEPATH")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("--append", action="store_true", help="Append path to MODULEPATH")
    p.add_argument("path", help="Path to add")

    p = parent.add_parser("unuse", help="Remove path from MODULEPATH")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("path", help="Path to remove")

    p = parent.add_parser("ignore", help="Ignore modules in this directory")
    p.add_argument("path", help="Path to ignore")

    pp = parent.add_parser("collection", help="Manage module collections")
    sp = pp.add_subparsers(dest="c_cmd", metavar="collection subcommands")
    p = sp.add_parser("save", help="Save the current modules to a collection")
    p.add_argument("name", nargs="?", default="default", help="Collection name")
    p = sp.add_parser("restore", help="Restore a module collection")
    p.add_argument("-c", action="store_true", help="Confirm before executing")
    p.add_argument("name", nargs="?", help="Collection name")
    p = sp.add_parser("show", help="Show module collection")
    p.add_argument("name", help="Collection name")
    p = sp.add_parser("remove", help="Remove module collection")
    p.add_argument("name", help="Collection name")
    sp.add_parser("avail", help="Show available module collections")

    p = parent.add_parser(
        "path",
        help="Subcommands for interacting with the MODULEPATH",
    )
    p.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        default=False,
        help="Show path and modules at each path entry",
    )
    sp = p.add_subparsers(dest="c_cmd", metavar="MODULEPATH subcommands")
    sp.add_parser("show", help="Show the MODULEPATH")
    p = sp.add_parser("append", help="Append PATH to MODULEPATH")
    p.add_argument("path", help="Path to append")
    p = sp.add_parser("prepend", help="Prepend PATH to MODULEPATH")
    p.add_argument("path", help="Path to prepend")

    p = parent.add_parser("meta", help="Subcommands for viewing module metadata")
    sp = p.add_subparsers(dest="c_cmd", metavar="meta subcommands")
    sp.add_parser("show", help="Show the environment meta data")

    p = parent.add_parser("cache", help="Interact with module cache")
    sp = p.add_subparsers(dest="c_cmd", metavar="modulecache subcommands")
    sp.add_parser("reset", help="Reset the module cache")
    sp.add_parser("refresh", help="Refresh module cache with current MODULEPATH")
    sp.add_parser("clear", help="Clear module cache")
    sp.add_parser("show", help="Show the module cache")

    p = parent.add_parser("guide", help="Display Modulecmd.py guides in the console")
    p.add_argument(
        "guide",
        choices=("intro", "basic_usage", "modulefile", "modulepath"),
        help="Name of guide to display",
    )

    p = parent.add_parser("visit", help="Change directory to path")
    p.add_argument("path", nargs="?", default="MODULECMD_PREFIX", help="Path to visit")

    pp = parent.add_parser("info", help="Display information")
    sp = pp.add_subparsers(dest="i_cmd", metavar="info subcommands")
    p = sp.add_parser("path", aliases=["modulepath"], help="Show modulepath")
    p.add_argument("-T", "--full", action="store_true", help="Show path and modules")
    p = sp.add_parser("module", help="Show module info")
    p.add_argument("name", help="Module name")

    return parser


def _main(parser, args):
    if args.subcommand == "__init__":
        __init__()
    elif args.subcommand == "find":
        find(args.name)
    elif args.subcommand == "avail":
        regex = None if not args.regex else " ".join(args.regex)
        avail(terse=args.terse, regex=regex, path=args.p)
    elif args.subcommand == "cat":
        cat(*args.names)
    elif args.subcommand == "load":
        load(*args.names, confirm=args.c)
    elif args.subcommand == "swap":
        load(args.name1, args.name2, confirm=args.c)
    elif args.subcommand == "unload":
        unload(*args.names, confirm=args.c)
    elif args.subcommand == "show":
        show(*args.names)
    elif args.subcommand == "purge":
        purge(confirm=args.c)
    elif args.subcommand in ("list", "ls"):
        list_loaded()
    elif args.subcommand == "config":
        if args.c_cmd == "show":
            print_config(show_path=args.p)
        elif args.c_cmd == "set":
            set_config_var(args.name, args.value)
        elif args.c_cmd == "pop":
            pop_config_var(args.name)
        else:
            parser.error("module config: missing required subcommand")
    elif args.subcommand == "help":
        print_help(args.name)
    elif args.subcommand == "reload":
        reload(args.name, confirm=args.c)
    elif args.subcommand == "whatis":
        print_whatis(args.name)
    elif args.subcommand == "use":
        use(args.path, append=args.append, confirm=args.c)
    elif args.subcommand == "unuse":
        unuse(args.path, confirm=args.c)
    elif args.subcommand == "ignore":
        ignore_modules(args.path)
    elif args.subcommand == "init":
        init(args.shell)
    elif args.subcommand == "collection":
        if args.c_cmd == "save":
            save_collection(args.name)
        elif args.c_cmd == "restore":
            restore_collection(args.name, confirm=args.c)
        elif args.c_cmd == "show":
            show_collection(args.name)
        elif args.c_cmd == "remove":
            remove_collection(args.name)
        elif args.c_cmd == "avail":
            show_available_collections()
        else:
            parser.error("module collection: missing required subcommand")
    elif args.subcommand == "path":
        if args.c_cmd == "show":
            show_modulepath(terse=not args.verbose)
        elif args.c_cmd == "prepend":
            use(args.path)
        elif args.c_cmd == "append":
            use(args.path, append=True)
        else:
            parser.error("module path: missing required subcommand")
    elif args.subcommand == "meta":
        if args.c_cmd == "show":
            show_meta_data()
    elif args.subcommand == "guide":
        show_guide(args.guide)
    elif args.subcommand == "cache":
        if args.c_cmd == "clear":
            clear_cache()
        elif args.c_cmd == "refresh":
            refresh_cache()
        elif args.c_cmd == "show":
            show_cache()
        elif args.c_cmd == "reset":
            reset_module_cache()
        else:
            parser.error("module cache: missing required subcommand")
    elif args.subcommand == "visit":
        visit_path(args.path)
    elif args.subcommand == "info":
        if args.i_cmd in ("path", "modulepath"):
            show_modulepath(terse=not args.full)
        elif args.i_cmd == "module":
            show_moduleinfo(args.name)
        else:
            parser.error("module info: missing required subcommand")
    else:
        parser.error("missing required subcommand")


def main_sourced(shell, *args_in) -> int:
    shell = shell.replace("shell.", "", 1)
    set_shell(shell)
    parser = modulecmd_parser()
    args = parser.parse_args(args_in)

    if args.debug:
        args.log_level = 3
        config.set("debug", True, "cmd_line")
        xio.set_debug(True)

    if args.log_level is not None:
        map: dict[int, int] = {
            0: xio.ERROR,
            1: xio.WARN,
            2: xio.INFO,
            3: xio.DEBUG,
            4: xio.TRACE,
        }
        level: int = map[args.log_level]
        xio.set_log_level(level)

    if args.dryrun:
        config.set("dryrun", True, "cmd_line")

    try:
        with MainContext(args.profile):
            return _main(parser, args)
    except Exception as e:
        if config.get("debug"):
            raise
        xio.die(str(e))
        return 1
    except KeyboardInterrupt:
        sys.stderr.write("\n")
        xio.die("Keyboard interrupt.")
        return 1
    except SystemExit as e:
        return int(e.code or 1)


def main_subshell(*args):
    parser = modulecmd_parser()
    args = parser.parse_args(args)
    if args.subcommand == "init":
        return init(args.shell)
    file = StringIO()
    file.write("Your shell has not been properly configured to use ")
    file.write(f"'modulecmd {args.subcommand}'.\n")
    file.write("To initialize your shell, run\n\n")
    file.write("$ module init <SHELL_NAME>\n\n")
    file.write("Currently supported shells are:\n")
    for shell in shells:
        file.write(f"- {shell}\n")
    file.write("See 'module init --help' for more information and options.\n\n")
    file.write("IMPORTANT: You may need to close and restart your ")
    file.write("shell after running 'conda init'.")
    print(file.getvalue())


def main(*args):
    args = args or sys.argv[1:]
    if args and args[0].strip().startswith("shell."):
        main = main_sourced
    else:
        main = main_subshell
    return main(*args)


if __name__ == "__main__":
    sys.exit(main())
