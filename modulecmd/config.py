import os
from configparser import ConfigParser
from io import StringIO
from typing import Any
from typing import Optional
from typing import TextIO
from typing import Union

try:
    import tkinter

    tkinter_imported = True
except ImportError:
    tkinter_imported = False

from .filesystem import mkdirp
from .util import singleton
from .util import split

default_settings = {
    "debug": False,
    "load_after_purge": [],
    "serialize_chunk_size": 1000,
    "use_cache": True,
    "use_tkinter": "auto",
}


def convert(arg, type):
    if type is list and isinstance(arg, str):
        return split(arg, sep=",")
    elif type is bool and isinstance(arg, str):
        return False if arg.lower() in ("0", "false", "off", "no", "") else True
    return type(arg)


def get_config_dir(disp: int = 0) -> Union[str, tuple[str, str]]:
    if "MODULECMD_CONFIG_DIR" in os.environ:
        scope = "env"
        config_dir = os.environ["MODULECMD_CONFIG_DIR"]
    elif "XDG_CONFIG_HOME" in os.environ:
        scope = "env"
        config_dir = os.environ["XDG_CONFIG_HOME"]
    else:
        scope = "default"
        config_dir = "~/.config/modulecmd"
    if "SNLSYSTEM" in os.environ:
        config_dir = os.path.join(config_dir, os.environ["SNLSYSTEM"])
    if disp:
        return scope, os.path.expanduser(config_dir)
    return os.path.expanduser(config_dir)


def get_cache_dir(disp: int = 0) -> Union[str, tuple[str, str]]:
    if "MODULECMD_CACHE_DIR" in os.environ:
        scope = "env"
        cache_dir = os.environ["MODULECMD_CACHE_DIR"]
    elif "MODULECMD_CONFIG_DIR" in os.environ:
        scope = "env"
        cache_dir = os.environ["MODULECMD_CONFIG_DIR"]
    elif "XDG_CACHE_HOME" in os.environ:
        scope = "env"
        cache_dir = os.environ["XDG_CACHE_HOME"]
    else:
        scope = "default"
        cache_dir = "~/.cache/modulecmd"
    if "SNLSYSTEM" in os.environ:
        cache_dir = os.path.join(cache_dir, os.environ["SNLSYSTEM"])
    if disp:
        return scope, os.path.expanduser(cache_dir)
    return os.path.expanduser(cache_dir)


class configuration:
    def __init__(self) -> None:
        self.data: dict[tuple[str, str], dict] = {}
        self.config_dir_scope, self.config_dir = get_config_dir(disp=1)  # type: ignore
        self.cache_dir_scope, self.cache_dir = get_cache_dir(disp=1)  # type: ignore
        self.config_file = os.path.join(self.config_dir, "config.ini")
        user_cfg = self.read_user_config()
        for key, value in default_settings.items():
            if f"MODULECMD_{key.upper()}" in os.environ:
                value = os.environ[f"MODULECMD_{key.upper()}"]
                self.set(key, value, "env")
            elif key in user_cfg:
                value = user_cfg[key]
                self.set(key, value, "user")
            else:
                self.set(key, value, "default")

    def read_user_config(self) -> dict:
        config = {}
        if os.path.exists(self.config_file):
            fd = ConfigParser()
            fd.read(self.config_file)
            if fd.has_section("modulecmd"):
                for key, val in fd.items("modulecmd"):
                    config[key] = val
        return config

    def set(self, name: str, value: Any, scope: str) -> None:
        if name not in default_settings:
            raise ValueError(f"Invalid setting: {name}")
        default_type = type(default_settings[name])
        if (name, value) == ("use_tkinter", "auto"):
            default_type = bool
            value = tkinter_imported
        key = (scope, name)
        if value is not None:
            value = convert(value, default_type)
        self.data[key] = value
        if name == "use_tkinter" and self.data[key] and not tkinter_imported:  # pragma: no cover
            raise RuntimeError(
                "use_tkinter requested but this python "
                "interpreter was not built with tkinter support"
            )

    def get(self, name: str, default: Any = None) -> tuple[Optional[str], Any]:
        for scope in ("cmd_line", "env", "user", "default"):
            if (scope, name) in self.data:
                return scope, self.data[(scope, name)]
        return None, default

    def pop(self, name: str, scope: str) -> None:
        self.data.pop((scope, name), None)

    @staticmethod
    def to_string(arg):
        if arg is None:
            return "None"
        elif isinstance(arg, bool):
            return "true" if arg else "false"
        elif isinstance(arg, (list, tuple)):
            return ", ".join(str(_) for _ in arg)
        return str(arg)

    def dump(self):
        mkdirp(self.config_dir)
        user_settings = [
            (name, val) for (scope, name), val in self.data.items() if scope == "user"
        ]
        if not user_settings:
            return
        p = ConfigParser()
        p.add_section("modulecmd")
        for name, value in user_settings:
            p["modulecmd"][name] = self.to_string(value)
        with open(self.config_file, "w") as fh:
            p.write(fh)


_config = singleton(configuration)


def get(key: str, default: Any = None) -> Any:
    """Module-level wrapper for ``Configuration.get()``."""
    _, value = _config.get(key, default=default)
    return value


def set(key: str, value: Any, scope: str) -> None:  # pragma: no cover
    """Convenience function for setting single values in config files."""
    _config.set(key, value, scope)
    if scope == "user":
        _config.dump()


def pop(key: str, scope: str) -> None:  # pragma: no cover
    """Convenience function for getting popping values from config files."""
    _config.pop(key, scope)
    if scope == "user":
        _config.dump()


def config_file() -> str:
    return _config.config_file


def config_dir() -> str:
    return _config.config_dir


def cache_dir() -> str:
    return _config.config_dir


def prettystr(file: Optional[TextIO] = None) -> Optional[str]:
    fown = file is None
    file = file or StringIO()
    table = [["name", "scope", "value"]]
    table.append(["cache_dir", _config.cache_dir_scope, _config.cache_dir])
    table.append(["config_dir", _config.config_dir_scope, _config.config_dir])
    for name in default_settings:
        scope, value = _config.get(name)
        value = _config.to_string(value)
        table.append([name, scope, value])
    m1 = max([len(row[0]) for row in table])
    m2 = max([len(row[1]) for row in table])
    m3 = min(30, max([len(row[2]) for row in table]))
    row = table[0]
    file.write("{0:{3}s} {1:{4}s} {2:s}\n".format(row[0], row[1], row[2], m1, m2))
    file.write("{0} {1} {2}\n".format("-" * m1, "-" * m2, "-" * m3))
    for row in table[1:]:
        name, scope, value = row
        file.write("{0:{3}s} {1:{4}s} {2:s}\n".format(name, scope, value, m1, m2))
    if fown:
        assert isinstance(file, StringIO)
        return file.getvalue()
    return None
