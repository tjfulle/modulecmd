import base64
import copy
import datetime
import json
import os
import zlib
from textwrap import wrap
from typing import Any
from typing import Callable
from typing import Generator
from typing import Optional
from typing import Union


def unique(a: list[Any]) -> Generator[Any, None, None]:
    unique_items = set()
    for item in a:
        if item not in unique_items:
            yield item
            unique_items.add(item)


def safe_int(a: str) -> Union[int, str]:
    try:
        return int(float(a))
    except ValueError:
        return a


def split(
    arg: Union[str, None],
    sep: Optional[str] = None,
    maxsplit: int = -1,
    transform: Optional[Callable] = None,
) -> list[Any]:
    if not arg:
        return []
    parts: list[str] = [_.strip() for _ in arg.split(sep, maxsplit=maxsplit) if _.split()]
    if transform is None:
        return parts
    return [transform(_) for _ in parts]


def join(arg: list[Any], sep: Optional[str] = None) -> str:
    if not arg:
        return ""
    if sep is None:
        sep = " "
    return sep.join(str(_) for _ in arg)


class singleton:
    """Simple wrapper for lazily initialized singleton objects."""

    def __init__(self, factory: Callable) -> None:
        """Create a new singleton to be initiated with the factory function.

        Args:
            factory (function): function taking no arguments that
                creates the singleton instance.
        """
        self.factory: Callable = factory
        self._instance: Any = None

    @property
    def instance(self) -> Any:
        if self._instance is None:
            self._instance = self.factory()
        return self._instance

    def __getattr__(self, name: str) -> Any:
        # When unpickling Singleton objects, the 'instance' attribute may be
        # requested but not yet set. The final 'getattr' line here requires
        # 'instance'/'_instance' to be defined or it will enter an infinite
        # loop, so protect against that here.
        if name in ["_instance", "instance"]:  # pragma: no cover
            raise AttributeError()
        return getattr(self.instance, name)

    def __getitem__(self, name: str) -> Any:
        return self.instance[name]

    def __contains__(self, element: Any) -> Any:
        return element in self.instance

    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        return self.instance(*args, **kwargs)

    def __iter__(self) -> Any:
        return iter(self.instance)

    def __str__(self) -> str:
        return str(self.instance)

    def __repr__(self) -> str:
        return repr(self.instance)


def deserialize(serialized: Union[str, list]) -> Any:
    if isinstance(serialized, list):
        serialized = "".join(serialized)
    string = _decode(serialized)
    return json.loads(string)


def serialize(obj: Any, chunk_size: Optional[int] = None) -> Union[str, list[str]]:
    serialized = _encode(json.dumps(obj))
    if chunk_size is None:
        return serialized
    elif chunk_size < 0:
        return [serialized]
    return wrap(serialized, chunk_size)


def _encode(item, compress: bool = True) -> str:
    encoded = str(item).encode("utf-8")
    if compress:
        encoded = zlib.compress(encoded)
    return base64.urlsafe_b64encode(encoded).decode()


def _decode(item, compress: bool = True) -> str:
    encoded = base64.urlsafe_b64decode(str(item))
    if compress:
        encoded = zlib.decompress(encoded)
    return encoded.decode()


def envbool(name: str) -> bool:
    if name not in os.environ:
        return False
    elif os.environ[name].lower() in ("0", "false", "no", "off", ""):
        return False
    return True


def read_tcl_default_version(filename: str) -> Optional[str]:
    with open(filename) as fh:
        for i, line in enumerate(fh.readlines()):
            line = " ".join(line.split())
            if i == 0 and not line.startswith("#%Module"):
                pass
            if line.startswith("set ModulesVersion"):
                raw_version = line.split("#", 1)[0].split()[-1]
                try:
                    version = eval(raw_version)
                except (SyntaxError, NameError):  # pragma: no cover
                    version = raw_version
                return version
    return None


def eval_bool_expr(expr: str) -> Optional[bool]:
    import os  # noqa: F401,E401
    import sys  # noqa: F401,E401

    # The above imports aren't used locally, but might be in the eval below
    try:
        return bool(eval(expr))
    except:  # noqa: E722
        return None


class checkpoint:
    checkpoints: dict[tuple[str, str], Any] = {}

    @classmethod
    def create(cls, obj):
        now = datetime.datetime.now()
        key = (obj.__class__.__name__, now.strftime("%c"))
        cls.checkpoints[key] = (copy.deepcopy(obj.__dict__), os.environ.copy())
        return key

    @classmethod
    def restore(cls, obj, checkpoint_id):
        data, os.environ = cls.checkpoints.pop(checkpoint_id)
        for key, val in data.items():
            obj.__dict__[key] = val

    @classmethod
    def delete(cls, obj, checkpoint_id):
        cls.checkpoints.pop(checkpoint_id, None)
